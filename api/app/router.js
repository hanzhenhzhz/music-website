'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller, middleware } = app;
  const authAdmin = middleware.authAdmin();
  const authUser = middleware.authUser();
  const userGetUid = middleware.userGetUid();
  /* 管理后台接口 */
  /* 管理员用户 */
  // 管理员登录
  router.post('/admin/account/login', controller.admin.account.login);
  // 管理员获取拥有权限的模块
  router.get('/admin/account/menu',authAdmin, controller.admin.account.menu);
  // 管理员修改自己密码
  router.post('/admin/account/editPassword',authAdmin, controller.admin.account.editPassword);
  // 管理员获取自己的信息
  router.get('/admin/account/getInfo',authAdmin, controller.admin.account.getInfo);
  // 管理员修改自己信息
  router.post('/admin/account/editInfo',authAdmin, controller.admin.account.editInfo);

  /* 功能类接口 */
  // 上传图片
  router.post('/admin/upload/image',authAdmin, controller.admin.upload.image);
  // 上传歌曲
  router.post('/admin/upload/music',authAdmin, controller.admin.upload.music);

  /* 首页 */
  // 首页数据统计
  router.get('/admin/home/statistical',authAdmin, controller.admin.home.statistical);

  /* 模块管理 */
  // 模块集合
  router.get('/admin/menu/list',authAdmin, controller.admin.menu.list);
  // 模块详情
  router.get('/admin/menu/detail',authAdmin, controller.admin.menu.detail);
  // 模块添加/编辑
  router.post('/admin/menu/form',authAdmin, controller.admin.menu.form);
  // 模块删除
  router.post('/admin/menu/del',authAdmin, controller.admin.menu.del);

  /* 角色管理 */
  // 角色列表
  router.get('/admin/role/page',authAdmin, controller.admin.role.page);
  // 角色详情
  router.get('/admin/role/detail',authAdmin, controller.admin.role.detail);
  // 角色添加/编辑
  router.post('/admin/role/form',authAdmin, controller.admin.role.form);
  // 角色删除
  router.post('/admin/role/del',authAdmin, controller.admin.role.del);
  // 添加/编辑时获取模块集合
  router.get('/admin/role/menuList',authAdmin, controller.admin.role.menuList);

  /* 管理员管理 */
  // 管理员列表
  router.get('/admin/admin/page',authAdmin, controller.admin.admin.page);
  // 管理员详情
  router.get('/admin/admin/detail',authAdmin, controller.admin.admin.detail);
  // 管理员添加/编辑
  router.post('/admin/admin/form',authAdmin, controller.admin.admin.form);
  // 管理员删除
  router.post('/admin/admin/del',authAdmin, controller.admin.admin.del);
  // 管理员启用禁用
  router.post('/admin/admin/status',authAdmin, controller.admin.admin.status);
  // 管理员修改密码
  router.post('/admin/admin/editPassword',authAdmin, controller.admin.admin.editPassword);
  // 添加/编辑、设置权限时获取模块集合
  router.get('/admin/admin/roleList',authAdmin, controller.admin.admin.roleList);

  /* 用户管理 */
  // 用户列表
  router.get('/admin/user/page',authAdmin, controller.admin.user.page);
  // 用户详情
  router.get('/admin/user/detail',authAdmin, controller.admin.user.detail);
  // 用户删除
  router.post('/admin/user/del',authAdmin, controller.admin.user.del);
  // 用户状态修改
  router.post('/admin/user/status',authAdmin, controller.admin.user.status);

  /* 广告管理 */
  // 广告列表
  router.get('/admin/banner/page',authAdmin, controller.admin.banner.page);
  // 广告详情
  router.get('/admin/banner/detail',authAdmin, controller.admin.banner.detail);
  // 广告添加/编辑
  router.post('/admin/banner/form',authAdmin, controller.admin.banner.form);
  // 广告删除
  router.post('/admin/banner/del',authAdmin, controller.admin.banner.del);
  // 广告状态修改
  router.post('/admin/banner/status',authAdmin, controller.admin.banner.status);

  /* 音乐分类管理 */
  // 音乐分类列表
  router.get('/admin/classify/page',authAdmin, controller.admin.classify.page);
  // 音乐分类详情
  router.get('/admin/classify/detail',authAdmin, controller.admin.classify.detail);
  // 音乐分类添加/编辑
  router.post('/admin/classify/form',authAdmin, controller.admin.classify.form);
  // 音乐分类删除
  router.post('/admin/classify/del',authAdmin, controller.admin.classify.del);
  // 音乐分类状态修改
  router.post('/admin/classify/status',authAdmin, controller.admin.classify.status);

  /* 歌星管理 */
  // 歌星列表
  router.get('/admin/star/page',authAdmin, controller.admin.star.page);
  // 歌星详情
  router.get('/admin/star/detail',authAdmin, controller.admin.star.detail);
  // 歌星添加/编辑
  router.post('/admin/star/form',authAdmin, controller.admin.star.form);
  // 歌星删除
  router.post('/admin/star/del',authAdmin, controller.admin.star.del);
  // 歌星状态修改
  router.post('/admin/star/status',authAdmin, controller.admin.star.status);
  // 歌星评论列表
  router.get('/admin/star/commentPage',authAdmin, controller.admin.star.commentPage);
  // 歌星评论详情
  router.get('/admin/star/commentDetail',authAdmin, controller.admin.star.commentDetail);
  // 歌星评论删除
  router.post('/admin/star/commentDel',authAdmin, controller.admin.star.commentDel);
  // 歌星评论状态修改
  router.post('/admin/star/commentStatus',authAdmin, controller.admin.star.commentStatus);

  /* 专辑管理 */
  // 专辑列表
  router.get('/admin/album/page',authAdmin, controller.admin.album.page);
  // 专辑详情
  router.get('/admin/album/detail',authAdmin, controller.admin.album.detail);
  // 专辑添加/编辑
  router.post('/admin/album/form',authAdmin, controller.admin.album.form);
  // 专辑删除
  router.post('/admin/album/del',authAdmin, controller.admin.album.del);
  // 专辑状态修改
  router.post('/admin/album/status',authAdmin, controller.admin.album.status);
  // 添加/编辑专辑获取歌星列表
  router.get('/admin/album/starList',authAdmin, controller.admin.album.starList);
  // 专辑评论列表
  router.get('/admin/album/commentPage',authAdmin, controller.admin.album.commentPage);
  // 专辑评论详情
  router.get('/admin/album/commentDetail',authAdmin, controller.admin.album.commentDetail);
  // 专辑评论删除
  router.post('/admin/album/commentDel',authAdmin, controller.admin.album.commentDel);
  // 专辑评论状态修改
  router.post('/admin/album/commentStatus',authAdmin, controller.admin.album.commentStatus);

  /* 歌曲管理 */
  // 歌曲列表
  router.get('/admin/music/page',authAdmin, controller.admin.music.page);
  // 歌曲详情
  router.get('/admin/music/detail',authAdmin, controller.admin.music.detail);
  // 歌曲添加/编辑
  router.post('/admin/music/form',authAdmin, controller.admin.music.form);
  // 歌曲删除
  router.post('/admin/music/del',authAdmin, controller.admin.music.del);
  // 歌曲状态修改
  router.post('/admin/music/status',authAdmin, controller.admin.music.status);
  // 添加/编辑歌曲获取分类列表
  router.get('/admin/music/classifyList',authAdmin, controller.admin.music.classifyList);
  // 添加/编辑歌曲获取歌星列表
  router.get('/admin/music/starList',authAdmin, controller.admin.music.starList);
  // 添加/编辑歌曲获取专辑列表
  router.get('/admin/music/albumList',authAdmin, controller.admin.music.albumList);
  // 歌曲评论列表
  router.get('/admin/music/commentPage',authAdmin, controller.admin.music.commentPage);
  // 歌曲评论详情
  router.get('/admin/music/commentDetail',authAdmin, controller.admin.music.commentDetail);
  // 歌曲评论删除
  router.post('/admin/music/commentDel',authAdmin, controller.admin.music.commentDel);
  // 歌曲评论状态修改
  router.post('/admin/music/commentStatus',authAdmin, controller.admin.music.commentStatus);

  /* 歌单管理 */
  // 歌单列表
  router.get('/admin/playlist/page',authAdmin, controller.admin.playlist.page);
  // 歌单详情
  router.get('/admin/playlist/detail',authAdmin, controller.admin.playlist.detail);
  // 歌单删除
  router.post('/admin/playlist/del',authAdmin, controller.admin.playlist.del);
  // 歌单状态修改
  router.post('/admin/playlist/status',authAdmin, controller.admin.playlist.status);
  // 歌单下歌曲列表
  router.get('/admin/playlist/musiclist',authAdmin, controller.admin.playlist.musiclist);
  // 歌单评论列表
  router.get('/admin/playlist/commentPage',authAdmin, controller.admin.playlist.commentPage);
  // 歌单评论详情
  router.get('/admin/playlist/commentDetail',authAdmin, controller.admin.playlist.commentDetail);
  // 歌单评论删除
  router.post('/admin/playlist/commentDel',authAdmin, controller.admin.playlist.commentDel);
  // 歌单评论状态修改
  router.post('/admin/playlist/commentStatus',authAdmin, controller.admin.playlist.commentStatus);

  /* 用户端接口 */
  /* 用户 */
  // 发送注册邮件
  router.post('/user/account/pushRegEmailCode', controller.user.account.pushRegEmailCode);
  // 注册
  router.post('/user/account/register', controller.user.account.register);
  // 登录
  router.post('/user/account/login', controller.user.account.login);
  // 账号详情
  router.get('/user/account/getInfo',authUser, controller.user.account.getInfo);
  // 账号信息修改
  router.post('/user/account/editInfo',authUser, controller.user.account.editInfo);
  // 修改密码
  router.post('/user/account/editPassword',authUser, controller.user.account.editPassword);
  // 发送找回密码邮件
  router.post('/user/account/pushFindEmailCode', controller.user.account.pushFindEmailCode);
  // 找回密码
  router.post('/user/account/findPassword', controller.user.account.findPassword);

  /* 功能类接口 */
  // 上传图片
  router.post('/user/upload/image',authUser, controller.user.upload.image);

  /* 广告 */
  // 获取广告
  router.get('/user/banner/getBanner',userGetUid, controller.user.banner.getBanner);

  /* 推荐 */
  // 首页推荐
  router.get('/user/recommend/recommend',userGetUid, controller.user.recommend.recommend);

  /* 搜索 */
  // 全文搜索
  router.get('/user/search/search',userGetUid, controller.user.search.search);

  /* 分类 */
  // 分类列表
  router.get('/user/classify/list',userGetUid, controller.user.classify.list);

  /* 歌星 */
  // 歌星列表
  router.get('/user/star/page',userGetUid, controller.user.star.page);
  // 歌星详情
  router.get('/user/star/detail',userGetUid, controller.user.star.detail);
  // 歌星评论列表
  router.get('/user/star/commentPage',userGetUid, controller.user.star.commentPage);
  // 歌星评论/回复
  router.post('/user/star/comment',authUser, controller.user.star.comment);
  // 歌星评论点赞
  router.post('/user/star/doCommentLikes',authUser, controller.user.star.doCommentLikes);
  // 歌星评论取消点赞
  router.post('/user/star/doClearCommentLikes',authUser, controller.user.star.doClearCommentLikes);
  // 歌星点赞
  router.post('/user/star/doLikes',authUser, controller.user.star.doLikes);
  // 歌星取消点赞
  router.post('/user/star/doClearLikes',authUser, controller.user.star.doClearLikes);
  // 歌星收藏
  router.post('/user/star/doCollection',authUser, controller.user.star.doCollection);
  // 歌星取消收藏
  router.post('/user/star/doClearCollection',authUser, controller.user.star.doClearCollection);
  // 收藏的歌星
  router.get('/user/star/collectionPage',authUser, controller.user.star.collectionPage);

  /* 专辑 */
  // 专辑列表
  router.get('/user/album/page',userGetUid, controller.user.album.page);
  // 专辑详情
  router.get('/user/album/detail',userGetUid, controller.user.album.detail);
  // 专辑评论列表
  router.get('/user/album/commentPage',userGetUid, controller.user.album.commentPage);
  // 专辑评论/回复
  router.post('/user/album/comment',authUser, controller.user.album.comment);
  // 专辑评论点赞
  router.post('/user/album/doCommentLikes',authUser, controller.user.album.doCommentLikes);
  // 专辑评论取消点赞
  router.post('/user/album/doClearCommentLikes',authUser, controller.user.album.doClearCommentLikes);
  // 专辑点赞
  router.post('/user/album/doLikes',authUser, controller.user.album.doLikes);
  // 专辑取消点赞
  router.post('/user/album/doClearLikes',authUser, controller.user.album.doClearLikes);
  // 专辑收藏
  router.post('/user/album/doCollection',authUser, controller.user.album.doCollection);
  // 专辑取消收藏
  router.post('/user/album/doClearCollection',authUser, controller.user.album.doClearCollection);
  // 收藏的专辑
  router.get('/user/album/collectionPage',authUser, controller.user.album.collectionPage);

  /* 歌曲 */
  // 歌曲列表
  router.get('/user/music/page',userGetUid, controller.user.music.page);
  // 歌曲详情
  router.get('/user/music/detail',userGetUid, controller.user.music.detail);
  // 歌曲播放量加1
  router.post('/user/music/addPlayNum',userGetUid, controller.user.music.addPlayNum);
  // 歌曲评论列表
  router.get('/user/music/commentPage',userGetUid, controller.user.music.commentPage);
  // 歌曲专辑评论/回复
  router.post('/user/music/comment',authUser, controller.user.music.comment);
  // 歌曲评论点赞
  router.post('/user/music/doCommentLikes',authUser, controller.user.music.doCommentLikes);
  // 歌曲评论取消点赞
  router.post('/user/music/doClearCommentLikes',authUser, controller.user.music.doClearCommentLikes);
  // 歌曲点赞
  router.post('/user/music/doLikes',authUser, controller.user.music.doLikes);
  // 歌曲取消点赞
  router.post('/user/music/doClearLikes',authUser, controller.user.music.doClearLikes);
  // 歌曲收藏
  router.post('/user/music/doCollection',authUser, controller.user.music.doCollection);
  // 歌曲取消收藏
  router.post('/user/music/doClearCollection',authUser, controller.user.music.doClearCollection);
  // 收藏的歌曲
  router.get('/user/music/collectionPage',authUser, controller.user.music.collectionPage);

  /* 歌单 */
  // 歌单列表
  router.get('/user/playlist/page',userGetUid, controller.user.playlist.page);
  // 歌单详情
  router.get('/user/playlist/detail',userGetUid, controller.user.playlist.detail);
  // 歌单播放量加1
  router.post('/user/playlist/addPlayNum',userGetUid, controller.user.playlist.addPlayNum);
  // 歌单评论列表
  router.get('/user/playlist/commentPage',userGetUid, controller.user.playlist.commentPage);
  // 歌单专辑评论/回复
  router.post('/user/playlist/comment',authUser, controller.user.playlist.comment);
  // 歌单评论点赞
  router.post('/user/playlist/doCommentLikes',authUser, controller.user.playlist.doCommentLikes);
  // 歌单评论取消点赞
  router.post('/user/playlist/doClearCommentLikes',authUser, controller.user.playlist.doClearCommentLikes);
  // 歌单点赞
  router.post('/user/playlist/doLikes',authUser, controller.user.playlist.doLikes);
  // 歌单取消点赞
  router.post('/user/playlist/doClearLikes',authUser, controller.user.playlist.doClearLikes);
  // 歌单收藏
  router.post('/user/playlist/doCollection',authUser, controller.user.playlist.doCollection);
  // 歌单取消收藏
  router.post('/user/playlist/doClearCollection',authUser, controller.user.playlist.doClearCollection);
  // 收藏的歌单
  router.get('/user/playlist/collectionPage',authUser, controller.user.playlist.collectionPage);
  // 歌单下歌曲列表
  router.get('/user/playlist/musicList',userGetUid, controller.user.playlist.musicList);
  // 我的歌单列表
  router.get('/user/playlist/myPage',authUser, controller.user.playlist.myPage);
  // 我的歌单详情
  router.get('/user/playlist/myDetail',authUser, controller.user.playlist.myDetail);
  // 歌单添加/编辑
  router.post('/user/playlist/form',authUser, controller.user.playlist.form);
  // 歌单删除
  router.post('/user/playlist/del',authUser, controller.user.playlist.del);
  // 歌单歌曲添加
  router.post('/user/playlist/addMusic',authUser, controller.user.playlist.addMusic);
  // 歌单歌曲删除
  router.post('/user/playlist/delMusic',authUser, controller.user.playlist.delMusic);
};
