/* indent size: 2 */
const moment = require('moment');
module.exports = app => {
  const DataTypes = app.Sequelize;

  const Collection = app.model.define('collection', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true
    },
    cid: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    type: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    uid: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    createTime: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'createTime',
      get(){
        return this.getDataValue('createTime')?moment(this.getDataValue('createTime')).format('YYYY-MM-DD HH:mm:ss'):'';
      }
    }
  }, {
    tableName: 'collection',
    timestamps: false
  });

  Collection.associate = function() {
    app.model.Collection.belongsTo(app.model.Star,{
      foreignKey: 'cid',
      targetKey: 'id',
      as:'star'
    });
    app.model.Collection.belongsTo(app.model.Album,{
      foreignKey: 'cid',
      targetKey: 'id',
      as:'album'
    });
    app.model.Collection.belongsTo(app.model.Music,{
      foreignKey: 'cid',
      targetKey: 'id',
      as:'music'
    });
    app.model.Collection.belongsTo(app.model.Playlist,{
      foreignKey: 'cid',
      targetKey: 'id',
      as:'playlist'
    });
    app.model.Collection.belongsTo(app.model.User,{
      foreignKey: 'uid',
      targetKey: 'id',
      as:'user'
    });
  }

  return Collection;
};
