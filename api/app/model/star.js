/* indent size: 2 */
const moment = require('moment');
module.exports = app => {
  const DataTypes = app.Sequelize;

  const Star = app.model.define('star', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    desc: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    headimg: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    status: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue:1
    },
    isDel: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'isDel',
      defaultValue:0
    },
    createTime: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'createTime',
      get(){
        return this.getDataValue('createTime')?moment(this.getDataValue('createTime')).format('YYYY-MM-DD HH:mm:ss'):'';
      }
    },
    updateTime: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'updateTime',
      get(){
        return this.getDataValue('updateTime')?moment(this.getDataValue('updateTime')).format('YYYY-MM-DD HH:mm:ss'):'';
      }
    },
    delTime: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'delTime',
      get(){
        return this.getDataValue('delTime')?moment(this.getDataValue('delTime')).format('YYYY-MM-DD HH:mm:ss'):'';
      }
    }
  }, {
    tableName: 'star',
    timestamps: false
  });

  Star.associate = function() {
    app.model.Star.hasMany(app.model.Collection,{
      foreignKey: 'cid',
      targetKey: 'id',
      as: 'collection'
    });
    app.model.Star.hasMany(app.model.Likes,{
      foreignKey: 'lid',
      targetKey: 'id',
      as: 'likes'
    });
    app.model.Star.hasMany(app.model.Comment,{
      foreignKey: 'cid',
      targetKey: 'id',
      as: 'comment'
    });
    app.model.Star.hasMany(app.model.Album,{
      foreignKey: 'sid',
      targetKey: 'id',
      as:'album'
    });
    app.model.Star.hasMany(app.model.Music,{
      foreignKey: 'sid',
      targetKey: 'id',
      as: 'music'
    });
  }

  return Star;
};
