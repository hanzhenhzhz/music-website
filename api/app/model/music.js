/* indent size: 2 */
const moment = require('moment');
module.exports = app => {
  const DataTypes = app.Sequelize;

  const Music = app.model.define('music', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    sid: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    aid: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    desc: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    cover: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    music: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    cid: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    lyrics: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    status: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue:1
    },
    isDel: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'isDel',
      defaultValue:0
    },
    playNum: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'playNum',
      defaultValue:0
    },
    createTime: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'createTime',
      get(){
        return this.getDataValue('createTime')?moment(this.getDataValue('createTime')).format('YYYY-MM-DD HH:mm:ss'):'';
      }
    },
    updateTime: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'updateTime',
      get(){
        return this.getDataValue('updateTime')?moment(this.getDataValue('updateTime')).format('YYYY-MM-DD HH:mm:ss'):'';
      }
    },
    delTime: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'delTime',
      get(){
        return this.getDataValue('delTime')?moment(this.getDataValue('delTime')).format('YYYY-MM-DD HH:mm:ss'):'';
      }
    }
  }, {
    tableName: 'music',
    timestamps: false
  });

  Music.associate = function() {
    app.model.Music.hasMany(app.model.Collection,{
      foreignKey: 'cid',
      targetKey: 'id',
      as: 'collection'
    });
    app.model.Music.hasMany(app.model.Likes,{
      foreignKey: 'lid',
      targetKey: 'id',
      as: 'likes'
    });
    app.model.Music.hasMany(app.model.Comment,{
      foreignKey: 'cid',
      targetKey: 'id',
      as: 'comment'
    });
    app.model.Music.belongsTo(app.model.Classify,{
      foreignKey: 'cid',
      targetKey: 'id',
      as:'classify'
    });
    app.model.Music.belongsTo(app.model.Star,{
      foreignKey: 'sid',
      targetKey: 'id',
      as:'star'
    });
    app.model.Music.belongsTo(app.model.Album,{
      foreignKey: 'aid',
      targetKey: 'id',
      as:'album'
    });
  }

  return Music;
};
