/* indent size: 2 */
const moment = require('moment');
module.exports = app => {
  const DataTypes = app.Sequelize;

  const Comment = app.model.define('comment', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true
    },
    cid: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    pid: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    content: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    type: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    uid: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    isDel: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'isDel',
      defaultValue:0
    },
    createTime: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'createTime',
      get(){
        return this.getDataValue('createTime')?moment(this.getDataValue('createTime')).format('YYYY-MM-DD HH:mm:ss'):'';
      }
    },
    updateTime: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'updateTime',
      get(){
        return this.getDataValue('updateTime')?moment(this.getDataValue('updateTime')).format('YYYY-MM-DD HH:mm:ss'):'';
      }
    },
    delTime: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'delTime',
      get(){
        return this.getDataValue('delTime')?moment(this.getDataValue('delTime')).format('YYYY-MM-DD HH:mm:ss'):'';
      }
    },
    status: {
      type: DataTypes.INTEGER(13),
      allowNull: false,
      defaultValue:1
    }
  }, {
    tableName: 'comment',
    timestamps: false
  });

  Comment.associate = function() {
    app.model.Comment.belongsTo(app.model.Star,{
      foreignKey: 'cid',
      targetKey: 'id',
      as:'star'
    });
    app.model.Comment.belongsTo(app.model.Album,{
      foreignKey: 'cid',
      targetKey: 'id',
      as:'album'
    });
    app.model.Comment.belongsTo(app.model.Music,{
      foreignKey: 'cid',
      targetKey: 'id',
      as:'music'
    });
    app.model.Comment.belongsTo(app.model.Playlist,{
      foreignKey: 'cid',
      targetKey: 'id',
      as:'playlist'
    });
    app.model.Comment.hasMany(app.model.Likes,{
      foreignKey: 'lid',
      targetKey: 'id',
      as: 'likes'
    });
    app.model.Comment.belongsTo(app.model.User,{
      foreignKey: 'uid',
      targetKey: 'id',
      as:'user'
    });
  }

  return Comment;
};
