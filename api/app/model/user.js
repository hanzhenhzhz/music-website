/* indent size: 2 */
const moment = require('moment');
module.exports = app => {
  const DataTypes = app.Sequelize;

  const User = app.model.define('user', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true
    },
    account: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    password: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    nickname: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    headimg: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    isDel: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'isDel',
      defaultValue:0
    },
    status: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue:1
    },
    createTime: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'createTime',
      get(){
        return this.getDataValue('createTime')?moment(this.getDataValue('createTime')).format('YYYY-MM-DD HH:mm:ss'):'';
      }
    },
    updateTime: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'updateTime',
      get(){
        return this.getDataValue('updateTime')?moment(this.getDataValue('updateTime')).format('YYYY-MM-DD HH:mm:ss'):'';
      }
    },
    delTime: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'delTime',
      get(){
        return this.getDataValue('delTime')?moment(this.getDataValue('delTime')).format('YYYY-MM-DD HH:mm:ss'):'';
      }
    }
  }, {
    tableName: 'user',
    timestamps: false
  });

  User.associate = function() {
    app.model.User.hasMany(app.model.Collection,{
      foreignKey: 'uid',
      targetKey: 'id',
      as: 'collection'
    });
    app.model.User.hasMany(app.model.Likes,{
      foreignKey: 'uid',
      targetKey: 'id',
      as: 'likes'
    });
    app.model.User.hasMany(app.model.Comment,{
      foreignKey: 'uid',
      targetKey: 'id',
      as: 'comment'
    });
    app.model.User.hasMany(app.model.Playlist,{
      foreignKey: 'uid',
      targetKey: 'id',
      as: 'playlist'
    });
  }

  return User;
};
