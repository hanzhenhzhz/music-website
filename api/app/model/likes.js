/* indent size: 2 */
const moment = require('moment');
module.exports = app => {
  const DataTypes = app.Sequelize;

  const Likes = app.model.define('likes', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true
    },
    lid: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    uid: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    type: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    createTime: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'createTime',
      get(){
        return this.getDataValue('createTime')?moment(this.getDataValue('createTime')).format('YYYY-MM-DD HH:mm:ss'):'';
      }
    }
  }, {
    tableName: 'likes',
    timestamps: false
  });

  Likes.associate = function() {
    app.model.Likes.belongsTo(app.model.Star,{
      foreignKey: 'lid',
      targetKey: 'id',
      as:'star'
    });
    app.model.Likes.belongsTo(app.model.Album,{
      foreignKey: 'lid',
      targetKey: 'id',
      as:'album'
    });
    app.model.Likes.belongsTo(app.model.Music,{
      foreignKey: 'lid',
      targetKey: 'id',
      as:'music'
    });
    app.model.Likes.belongsTo(app.model.Playlist,{
      foreignKey: 'lid',
      targetKey: 'id',
      as:'playlist'
    });
    app.model.Likes.belongsTo(app.model.Comment,{
      foreignKey: 'lid',
      targetKey: 'id',
      as:'comment'
    });
    app.model.Likes.belongsTo(app.model.User,{
      foreignKey: 'uid',
      targetKey: 'id',
      as:'user'
    });
  }

  return Likes;
};
