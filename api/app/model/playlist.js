/* indent size: 2 */
const moment = require('moment');
module.exports = app => {
  const DataTypes = app.Sequelize;

  const Playlist = app.model.define('playlist', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    desc: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    cover: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    status: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue:1
    },
    private: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    musics: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    isDel: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'isDel',
      defaultValue:0
    },
    uid: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    playNum: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'playNum',
      defaultValue:0
    },
    createTime: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'createTime',
      get(){
        return this.getDataValue('createTime')?moment(this.getDataValue('createTime')).format('YYYY-MM-DD HH:mm:ss'):'';
      }
    },
    updateTime: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'updateTime',
      get(){
        return this.getDataValue('updateTime')?moment(this.getDataValue('updateTime')).format('YYYY-MM-DD HH:mm:ss'):'';
      }
    },
    delTime: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'delTime',
      get(){
        return this.getDataValue('delTime')?moment(this.getDataValue('delTime')).format('YYYY-MM-DD HH:mm:ss'):'';
      }
    }
  }, {
    tableName: 'playlist',
    timestamps: false
  });

  Playlist.associate = function() {
    app.model.Playlist.hasMany(app.model.Collection,{
      foreignKey: 'cid',
      targetKey: 'id',
      as: 'collection'
    });
    app.model.Playlist.hasMany(app.model.Likes,{
      foreignKey: 'lid',
      targetKey: 'id',
      as: 'likes'
    });
    app.model.Playlist.hasMany(app.model.Comment,{
      foreignKey: 'cid',
      targetKey: 'id',
      as: 'comment'
    });
    app.model.Playlist.belongsTo(app.model.User,{
      foreignKey: 'uid',
      targetKey: 'id',
      as:'user'
    });
  }

  return Playlist;
};
