/* indent size: 2 */
const moment = require('moment');
module.exports = app => {
  const DataTypes = app.Sequelize;

  const Admin = app.model.define('admin', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,autoIncrement: true,
      autoIncrement: true
    },
    account: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    password: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    nickname: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    headimg: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    rid: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    isDel: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'isDel',
      defaultValue:0
    },
    status: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue:1
    },
    createTime: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'createTime',
      get(){
        return this.getDataValue('createTime')?moment(this.getDataValue('createTime')).format('YYYY-MM-DD HH:mm:ss'):'';
      }
    },
    updateTime: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'updateTime',
      get(){
        return this.getDataValue('updateTime')?moment(this.getDataValue('updateTime')).format('YYYY-MM-DD HH:mm:ss'):'';
      }
    },
    delTime: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'delTime',
      get(){
        return this.getDataValue('delTime')?moment(this.getDataValue('delTime')).format('YYYY-MM-DD HH:mm:ss'):'';
      }
    }
  }, {
    tableName: 'admin',
    timestamps: false
  });

  Admin.associate = function() {
    app.model.Admin.belongsTo(app.model.Role,{
      foreignKey: 'rid',
      targetKey: 'id',
      as:'role'
    });
  }

  return Admin;
};
