/* indent size: 2 */
module.exports = app => {
  const DataTypes = app.Sequelize;

  const Emailcode = app.model.define('emailcode', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true
    },
    email: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    code: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    type: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    verifyTime: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'verifyTime'
    },
    pushTime: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'pushTime'
    },
    isInvalid: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'isInvalid',
      defaultValue:0
    }
  }, {
    tableName: 'emailcode',
    timestamps: false
  });

  Emailcode.associate = function() {

  }

  return Emailcode;
};
