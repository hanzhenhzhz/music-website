/* indent size: 2 */
const moment = require('moment');
module.exports = app => {
  const DataTypes = app.Sequelize;

  const Album = app.model.define('album', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    desc: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    cover: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    sid: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    isDel: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'isDel',
      defaultValue:0
    },
    status: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue:1
    },
    createTime: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'createTime',
      get(){
        return this.getDataValue('createTime')?moment(this.getDataValue('createTime')).format('YYYY-MM-DD HH:mm:ss'):'';
      }
    },
    updateTime: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'updateTime',
      get(){
        return this.getDataValue('updateTime')?moment(this.getDataValue('updateTime')).format('YYYY-MM-DD HH:mm:ss'):'';
      }
    },
    delTime: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'delTime',
      get(){
        return this.getDataValue('delTime')?moment(this.getDataValue('delTime')).format('YYYY-MM-DD HH:mm:ss'):'';
      }
    }
  }, {
    tableName: 'album',
    timestamps: false
  });

  Album.associate = function() {
    app.model.Album.hasMany(app.model.Collection,{
      foreignKey: 'cid',
      targetKey: 'id',
      as: 'collection'
    });
    app.model.Album.hasMany(app.model.Likes,{
      foreignKey: 'lid',
      targetKey: 'id',
      as: 'likes'
    });
    app.model.Album.hasMany(app.model.Comment,{
      foreignKey: 'cid',
      targetKey: 'id',
      as: 'comment'
    });
    app.model.Album.hasMany(app.model.Music,{
      foreignKey: 'aid',
      targetKey: 'id',
      as: 'music'
    });
    app.model.Album.belongsTo(app.model.Star,{
      foreignKey: 'sid',
      targetKey: 'id',
      as:'star'
    });
  }

  return Album;
};
