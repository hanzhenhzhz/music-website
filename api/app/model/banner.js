/* indent size: 2 */
const moment = require('moment');
module.exports = app => {
  const DataTypes = app.Sequelize;

  const Banner = app.model.define('banner', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true
    },
    identity: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    desc: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    banners: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    status: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue:1
    },
    isDel: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'isDel',
      defaultValue:0
    },
    createTime: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'createTime',
      get(){
        return this.getDataValue('createTime')?moment(this.getDataValue('createTime')).format('YYYY-MM-DD HH:mm:ss'):'';
      }
    },
    updateTime: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'updateTime',
      get(){
        return this.getDataValue('updateTime')?moment(this.getDataValue('updateTime')).format('YYYY-MM-DD HH:mm:ss'):'';
      }
    },
    delTime: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'delTime',
      get(){
        return this.getDataValue('delTime')?moment(this.getDataValue('delTime')).format('YYYY-MM-DD HH:mm:ss'):'';
      }
    }
  }, {
    tableName: 'banner',
    timestamps: false
  });

  Banner.associate = function() {

  }

  return Banner;
};
