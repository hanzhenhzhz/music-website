'use strict';

const Controller = require('egg').Controller;

class BannerController extends Controller {
  // 广告列表
  async page() {
    const { ctx } = this;
    let {count,rows} = await ctx.service.admin.banner.page(ctx.query);
    return ctx.helper.resSuccess({
      total:count,
      data:rows
    });
  }

  // 广告详情
  async detail() {
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.query)){
      return ctx.helper.resError('请求参数有误');
    }
    let detail = await ctx.service.admin.banner.detail(ctx.query);
    if(detail){
      return ctx.helper.resSuccess(detail);
    }else{
      return ctx.helper.resError('获取信息失败');
    }
  }

  // 广告添加/编辑
  async form() {
    const { app,ctx } = this;
    if(app.validator.validate({name:'string'},ctx.request.body)){
      return ctx.helper.resError('广告名不能为空');
    }
    if(app.validator.validate({identity:'string'},ctx.request.body)){
        return ctx.helper.resError('唯一标识不能为空');
    }
    if(app.validator.validate({status:'int'},ctx.request.body)){
        return ctx.helper.resError('状态不能为空');
    }
    if(app.validator.validate({banners:'string'},ctx.request.body)){
      return ctx.helper.resError('广告内容不能为空');
    }
    let detail = await ctx.service.admin.banner.getIdentitydetail(ctx.request.body);
    if(detail&&detail.id!=ctx.request.body.id){
      return ctx.helper.resError('唯一标识不能重复');
    }
    if(ctx.request.body.id){
      let result = await ctx.service.admin.banner.edit(ctx.request.body);
      if(result[0]){
        return ctx.helper.resSuccess(true);
      }else{
        return ctx.helper.resError('更新失败');
      }
    }else{
      let result = await ctx.service.admin.banner.add(ctx.request.body);
      if(result.id){
        return ctx.helper.resSuccess(result.id);
      }else{
        return ctx.helper.resError('添加失败');
      }
    }
  }

  // 广告删除
  async del() {
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let result = await ctx.service.admin.banner.del(ctx.request.body);
    if(result[0]){
      return ctx.helper.resSuccess(true);
    }else{
      return ctx.helper.resError('删除失败');
    }
  }

  // 广告状态修改
  async status(){
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.request.body)||app.validator.validate({status:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let result = await ctx.service.admin.banner.status(ctx.request.body);
    if(result[0]){
      return ctx.helper.resSuccess(true);
    }else{
      return ctx.helper.resError('更新失败');
    }
  }
}

module.exports = BannerController;
