'use strict';

const Controller = require('egg').Controller;
const crypto = require('crypto');

class AccountController extends Controller {
  // 管理员登录
  async login() {
    const { app,ctx } = this;
    if(app.validator.validate({account:'string'},ctx.request.body)){
      return ctx.helper.resError('账号不能为空');
    }
    if(app.validator.validate({password:'string'},ctx.request.body)){
      return ctx.helper.resError('密码不能为空');
    }
    ctx.request.body.password = crypto.createHash('md5').update(`${app.config.crypto.secret}${ctx.request.body.password}`).digest('hex');
    let result = await ctx.service.admin.admin.login(ctx.request.body);
    if(result){
      if(result.status==1){
        return ctx.helper.resSuccess(
          app.jwt.sign({
            id:result.id,
            rid:result.rid,
            utype:'admin'
          }, this.app.config.jwt.secret, { expiresIn: '86400s' })
        );
      }else{
        return ctx.helper.resError('账号被禁用');
      }
    }else{
      return ctx.helper.resError('账号或密码错误');
    }
  }

  // 管理员获取拥有权限的模块
  async menu() {
    const { ctx } = this;
    return ctx.helper.resSuccess(await ctx.service.admin.role.roleMenuList({id:ctx.request.header.rid}));
  }

  // 管理员修改自己密码
  async editPassword(){
    const { app,ctx } = this;
    if(app.validator.validate({password:'string'},ctx.request.body)){
      return ctx.helper.resError('新密码不能为空');
    }
    if(app.validator.validate({password2:'string'},ctx.request.body)){
      return ctx.helper.resError('确认密码不能为空');
    }
    if(ctx.request.body.password!=ctx.request.body.password2){
      return ctx.helper.resError('确认密码不一致');
    }
    ctx.request.body.password = crypto.createHash('md5').update(`${app.config.crypto.secret}${ctx.request.body.password}`).digest('hex');
    ctx.request.body.id = ctx.request.header.uid;
    let result = await ctx.service.admin.admin.editPassword(ctx.request.body);
    if(result[0]){
      return ctx.helper.resSuccess(true);
    }else{
      return ctx.helper.resError('更新失败');
    }
  }

  // 管理员获取自己的信息
  async getInfo(){
    const { ctx } = this;
    let detail = await ctx.service.admin.admin.detail({id:ctx.request.header.uid});
    if(detail){
      return ctx.helper.resSuccess(detail);
    }else{
      return ctx.helper.resError('获取信息失败');
    }
  }

  // 管理员修改自己信息
  async editInfo(){
    const { app,ctx } = this;
    if(app.validator.validate({account:'string'},ctx.request.body)){
      return ctx.helper.resError('账号不能为空');
    }
    if(app.validator.validate({nickname:'string'},ctx.request.body)){
      return ctx.helper.resError('昵称不能为空');
    }
    let detail = await ctx.service.admin.admin.getAccountdetail(ctx.request.body);
    if(detail&&detail.id!=ctx.request.header.uid){
      return ctx.helper.resError('账号不能重复');
    }
    ctx.request.body.id = ctx.request.header.uid;
    let result = await ctx.service.admin.admin.editInfo(ctx.request.body);
    if(result[0]){
      return ctx.helper.resSuccess(true);
    }else{
      return ctx.helper.resError('更新失败');
    }
  }
}

module.exports = AccountController;
