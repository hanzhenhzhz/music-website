'use strict';

const Controller = require('egg').Controller;

class MenuController extends Controller {
  // 模块集合
  async list() {
    const { ctx } = this;
    let result = await ctx.service.admin.menu.list(ctx.query);
    return ctx.helper.resSuccess(result);
  }

  // 模块详情
  async detail(){
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.query)){
      return ctx.helper.resError('请求参数有误');
    }
    let detail = await ctx.service.admin.menu.detail(ctx.query);
    if(detail){
      return ctx.helper.resSuccess(detail);
    }else{
      return ctx.helper.resError('获取信息失败');
    }
  }

  // 模块添加/编辑
  async form(){
    const { app,ctx } = this;
    if(app.validator.validate({name:'string'},ctx.request.body)){
      return ctx.helper.resError('名称不能为空');
    }
    if(app.validator.validate({identity:'string'},ctx.request.body)){
      return ctx.helper.resError('唯一标识不能为空');
    }
    if(app.validator.validate({pid:'int'},ctx.request.body)){
      return ctx.helper.resError('父级不能为空');
    }
    let detail = await ctx.service.admin.menu.getIdentitydetail(ctx.request.body);
    if(detail&&detail.id!=ctx.request.body.id){
      return ctx.helper.resError('唯一标识不能重复');
    }
    if(ctx.request.body.id){
      let result = await ctx.service.admin.menu.edit(ctx.request.body);
      if(result[0]){
        return ctx.helper.resSuccess(true);
      }else{
        return ctx.helper.resError('更新失败');
      }
    }else{
      let result = await ctx.service.admin.menu.add(ctx.request.body);
      if(result.id){
        return ctx.helper.resSuccess(result.id);
      }else{
        return ctx.helper.resError('添加失败');
      }
    }
  }

  // 模块删除
  async del(){
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let result = await ctx.service.admin.menu.del(ctx.request.body);
    if(result[0]){
      return ctx.helper.resSuccess(true);
    }else{
      return ctx.helper.resError('删除失败');
    }
  }
}

module.exports = MenuController;
