'use strict';

const Controller = require('egg').Controller;

class MusicController extends Controller {
  // 歌曲列表
  async page() {
    const { ctx } = this;
    let {count,rows} = await ctx.service.admin.music.page(ctx.query);
    return ctx.helper.resSuccess({
      total:count,
      data:rows
    });
  }

  // 歌曲详情
  async detail() {
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.query)){
      return ctx.helper.resError('请求参数有误');
    }
    let detail = await ctx.service.admin.music.detail(ctx.query);
    if(detail){
      return ctx.helper.resSuccess(detail);
    }else{
      return ctx.helper.resError('获取信息失败');
    }
  }

  // 歌曲添加/编辑
  async form() {
    const { app,ctx } = this;
    if(app.validator.validate({name:'string'},ctx.request.body)){
      return ctx.helper.resError('歌曲名不能为空');
    }
    if(app.validator.validate({music:'string'},ctx.request.body)){
      return ctx.helper.resError('歌曲链接不能为空');
    }
    if(app.validator.validate({cid:'int'},ctx.request.body)){
      return ctx.helper.resError('所属分类不能为空');
    }
    if(app.validator.validate({status:'int'},ctx.request.body)){
      return ctx.helper.resError('状态不能为空');
    }
    if(ctx.request.body.id){
      let result = await ctx.service.admin.music.edit(ctx.request.body);
      if(result[0]){
        return ctx.helper.resSuccess(true);
      }else{
        return ctx.helper.resError('更新失败');
      }
    }else{
      let result = await ctx.service.admin.music.add(ctx.request.body);
      if(result.id){
        return ctx.helper.resSuccess(result.id);
      }else{
        return ctx.helper.resError('添加失败');
      }
    }
  }

  // 歌曲删除
  async del() {
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let result = await ctx.service.admin.music.del(ctx.request.body);
    if(result[0]){
      return ctx.helper.resSuccess(true);
    }else{
      return ctx.helper.resError('删除失败');
    }
  }

  // 歌曲状态修改
  async status(){
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.request.body)||app.validator.validate({status:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let result = await ctx.service.admin.music.status(ctx.request.body);
    if(result[0]){
      return ctx.helper.resSuccess(true);
    }else{
      return ctx.helper.resError('更新失败');
    }
  }

  // 添加/编辑歌曲获取分类列表
  async classifyList() {
    const { ctx } = this;
    if(ctx.query.pageNO){
      let {count,rows} = await ctx.service.admin.music.classifyList(ctx.query);
      return ctx.helper.resSuccess({
        total:count,
        data:rows
      });
    }else{
      let result = await ctx.service.admin.music.classifyList(ctx.query);
      return ctx.helper.resSuccess(result);
    }
  }

  // 添加/编辑歌曲获取歌星列表
  async starList() {
    const { ctx } = this;
    if(ctx.query.pageNO){
      let {count,rows} = await ctx.service.admin.music.starList(ctx.query);
      return ctx.helper.resSuccess({
        total:count,
        data:rows
      });
    }else{
      let result = await ctx.service.admin.music.starList(ctx.query);
      return ctx.helper.resSuccess(result);
    }
  }

  // 添加/编辑歌曲获取专辑列表
  async albumList() {
    const { ctx } = this;
    if(ctx.query.pageNO){
      let {count,rows} = await ctx.service.admin.music.albumList(ctx.query);
      return ctx.helper.resSuccess({
        total:count,
        data:rows
      });
    }else{
      let result = await ctx.service.admin.music.albumList(ctx.query);
      return ctx.helper.resSuccess(result);
    }
  }

  // 歌曲评论列表
  async commentPage() {
    const { app,ctx } = this;
    if(app.validator.validate({cid:'int'},ctx.query)){
      return ctx.helper.resError('请求参数有误');
    }
    let {count,rows} = await ctx.service.admin.music.commentPage(ctx.query);
    return ctx.helper.resSuccess({
      total:count,
      data:rows
    });
  }

  // 歌曲评论详情
  async commentDetail() {
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.query)){
      return ctx.helper.resError('请求参数有误');
    }
    let detail = await ctx.service.admin.music.commentDetail(ctx.query);
    if(detail){
      return ctx.helper.resSuccess(detail);
    }else{
      return ctx.helper.resError('获取信息失败');
    }
  }

  // 歌曲评论删除
  async commentDel() {
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let result = await ctx.service.admin.music.commentDel(ctx.request.body);
    if(result[0]){
      return ctx.helper.resSuccess(true);
    }else{
      return ctx.helper.resError('删除失败');
    }
  }

  // 歌曲评论状态修改
  async commentStatus(){
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.request.body)||app.validator.validate({status:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let result = await ctx.service.admin.music.commentStatus(ctx.request.body);
    if(result[0]){
      return ctx.helper.resSuccess(true);
    }else{
      return ctx.helper.resError('更新失败');
    }
  }
}

module.exports = MusicController;
