'use strict';

const Sequelize = require('sequelize');
const Controller = require('egg').Controller;

class StarController extends Controller {
  // 歌星列表
  async page() {
    const { ctx } = this;
    let {count,rows} = await ctx.service.admin.star.page(ctx.query);
    return ctx.helper.resSuccess({
      total:count,
      data:rows
    });
  }

  // 歌星详情
  async detail() {
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.query)){
      return ctx.helper.resError('请求参数有误');
    }
    let detail = await ctx.service.admin.star.detail(ctx.query);
    if(detail){
      return ctx.helper.resSuccess(detail);
    }else{
      return ctx.helper.resError('获取信息失败');
    }
  }

  // 歌星添加/编辑
  async form() {
    const { app,ctx } = this;
    if(app.validator.validate({name:'string'},ctx.request.body)){
      return ctx.helper.resError('姓名不能为空');
    }
    if(app.validator.validate({status:'int'},ctx.request.body)){
        return ctx.helper.resError('状态不能为空');
    }
    if(ctx.request.body.id){
      let result = await ctx.service.admin.star.edit(ctx.request.body);
      if(result[0]){
        return ctx.helper.resSuccess(true);
      }else{
        return ctx.helper.resError('更新失败');
      }
    }else{
      let result = await ctx.service.admin.star.add(ctx.request.body);
      if(result.id){
        return ctx.helper.resSuccess(result.id);
      }else{
        return ctx.helper.resError('添加失败');
      }
    }
  }

  // 歌星删除
  async del() {
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let result = await ctx.service.admin.star.del(ctx.request.body);
    if(result[0]){
      return ctx.helper.resSuccess(true);
    }else{
      return ctx.helper.resError('删除失败');
    }
  }

  // 歌星状态修改
  async status(){
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.request.body)||app.validator.validate({status:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let result = await ctx.service.admin.star.status(ctx.request.body);
    if(result[0]){
      return ctx.helper.resSuccess(true);
    }else{
      return ctx.helper.resError('更新失败');
    }
  }

  // 歌星评论列表
  async commentPage() {
    const { app,ctx } = this;
    if(app.validator.validate({cid:'int'},ctx.query)){
      return ctx.helper.resError('请求参数有误');
    }
    let {count,rows} = await ctx.service.admin.star.commentPage(ctx.query);
    return ctx.helper.resSuccess({
      total:count,
      data:rows
    });
  }

  // 歌星评论详情
  async commentDetail() {
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.query)){
      return ctx.helper.resError('请求参数有误');
    }
    let detail = await ctx.service.admin.star.commentDetail(ctx.query);
    if(detail){
      return ctx.helper.resSuccess(detail);
    }else{
      return ctx.helper.resError('获取信息失败');
    }
  }

  // 歌星评论删除
  async commentDel() {
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let result = await ctx.service.admin.star.commentDel(ctx.request.body);
    if(result[0]){
      return ctx.helper.resSuccess(true);
    }else{
      return ctx.helper.resError('删除失败');
    }
  }

  // 歌星评论状态修改
  async commentStatus(){
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.request.body)||app.validator.validate({status:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let result = await ctx.service.admin.star.commentStatus(ctx.request.body);
    if(result[0]){
      return ctx.helper.resSuccess(true);
    }else{
      return ctx.helper.resError('更新失败');
    }
  }
}

module.exports = StarController;
