'use strict';

const Controller = require('egg').Controller;

class HomeController extends Controller {
  // 首页数据统计
  async statistical() {
    const { ctx } = this;
    let userNum = await ctx.service.admin.user.getNum();
    let musicNum = await ctx.service.admin.music.getNum();
    let starNum = await ctx.service.admin.star.getNum();
    let albumNum = await ctx.service.admin.album.getNum();
    let playlistNum = await ctx.service.admin.playlist.getNum();
    let topMusic = await ctx.service.admin.music.getTopMusic();
    let topPlaylist = await ctx.service.admin.playlist.getTopPlaylist();
    let albumPlayNum = await ctx.service.admin.album.getAlbumPlayNum();
    albumPlayNum.sort((a,b)=>{return b.playNum-a.playNum});
    let topAlbum = albumPlayNum.slice(0,10);
    let starPlayNum = await ctx.service.admin.star.getStarPlayNum();
    starPlayNum.sort((a,b)=>{return b.playNum-a.playNum});
    let topStar = starPlayNum.slice(0,10);
    return ctx.helper.resSuccess({
      userNum:userNum.dataValues.num,
      musicNum:musicNum.dataValues.num,
      starNum:starNum.dataValues.num,
      albumNum:albumNum.dataValues.num,
      playlistNum:playlistNum.dataValues.num,
      topMusic,
      topPlaylist,
      topAlbum,
      topStar
    });
  }
}

module.exports = HomeController;
