'use strict';

const Controller = require('egg').Controller;

class RoleController extends Controller {
  // 角色列表
  async page() {
    const { ctx } = this;
    let {count,rows} = await ctx.service.admin.role.page(ctx.query);
    return ctx.helper.resSuccess({
      total:count,
      data:rows
    });
  }

  // 角色详情
  async detail() {
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.query)){
      return ctx.helper.resError('请求参数有误');
    }
    let detail = await ctx.service.admin.role.detail(ctx.query);
    if(detail){
      return ctx.helper.resSuccess(detail);
    }else{
      return ctx.helper.resError('获取信息失败');
    }
  }

  // 角色添加/编辑
  async form() {
    const { app,ctx } = this;
    if(app.validator.validate({name:'string'},ctx.request.body)){
      return ctx.helper.resError('角色名不能为空');
    }
    if(ctx.request.body.power&&app.validator.validate({name:'array'},ctx.request.body)){
      return ctx.helper.resError('权限格式错误');
    }
    if(ctx.request.body.id==1){
      return ctx.helper.resError('不能修改超级管理员');
    }
    if(ctx.request.body.id){
      let result = await ctx.service.admin.role.edit(ctx.request.body);
      if(result[0]){
        return ctx.helper.resSuccess(true);
      }else{
        return ctx.helper.resError('更新失败');
      }
    }else{
      let result = await ctx.service.admin.role.add(ctx.request.body);
      if(result.id){
        return ctx.helper.resSuccess(result.id);
      }else{
        return ctx.helper.resError('添加失败');
      }
    }
  }

  // 角色删除
  async del() {
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    if(ctx.request.body.id==1){
      return ctx.helper.resError('不能删除超级管理员');
    }
    let result = await ctx.service.admin.role.del(ctx.request.body);
    if(result[0]){
      return ctx.helper.resSuccess(true);
    }else{
      return ctx.helper.resError('删除失败');
    }
  }

  // 添加/编辑时获取模块集合
  async menuList() {
    const { ctx } = this;
    let result = await ctx.service.admin.role.menuList();
    return ctx.helper.resSuccess(result);
  }
}

module.exports = RoleController;
