'use strict';

const Controller = require('egg').Controller;
const crypto = require('crypto');

class AdminController extends Controller {
  // 管理员列表
  async page() {
    const { ctx } = this;
    let {count,rows} = await ctx.service.admin.admin.page(ctx.query);
    return ctx.helper.resSuccess({
      total:count,
      data:rows
    });
  }

  // 管理员详情
  async detail(){
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.query)){
      return ctx.helper.resError('请求参数有误');
    }
    let detail = await ctx.service.admin.admin.detail(ctx.query);
    if(detail){
      return ctx.helper.resSuccess(detail);
    }else{
      return ctx.helper.resError('获取信息失败');
    }
  }

  // 管理员添加编辑
  async form(){
    const { app,ctx } = this;
    if(app.validator.validate({account:'string'},ctx.request.body)){
      return ctx.helper.resError('账号不能为空');
    }
    if(!ctx.request.id&&app.validator.validate({password:'string'},ctx.request.body)){
      return ctx.helper.resError('密码不能为空');
    }
    if(app.validator.validate({nickname:'string'},ctx.request.body)){
      return ctx.helper.resError('昵称不能为空');
    }
    if(app.validator.validate({rid:'int'},ctx.request.body)){
      return ctx.helper.resError('角色不能为空');
    }
    if(app.validator.validate({status:'int'},ctx.request.body)){
      return ctx.helper.resError('状态不能为空');
    }
    if(ctx.request.body.id==1){
      return ctx.helper.resError('不能修改超级管理员');
    }
    let detail = await ctx.service.admin.admin.getAccountdetail(ctx.request.body);
    if(detail&&detail.id!=ctx.request.body.id){
      return ctx.helper.resError('账号不能重复');
    }
    if(ctx.request.body.id){
      let result = await ctx.service.admin.admin.edit(ctx.request.body);
      if(result[0]){
        return ctx.helper.resSuccess(true);
      }else{
        return ctx.helper.resError('更新失败');
      }
    }else{
      ctx.request.body.password = crypto.createHash('md5').update(`${app.config.crypto.secret}${ctx.request.body.password}`).digest('hex');
      let result = await ctx.service.admin.admin.add(ctx.request.body);
      if(result.id){
        return ctx.helper.resSuccess(result.id);
      }else{
        return ctx.helper.resError('添加失败');
      }
    }
  }

  // 管理员删除
  async del(){
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    if(ctx.request.body.id==1){
      return ctx.helper.resError('不能删除超级管理员');
    }
    let result = await ctx.service.admin.admin.del(ctx.request.body);
    if(result[0]){
      return ctx.helper.resSuccess(true);
    }else{
      return ctx.helper.resError('删除失败');
    }
  }

  // 管理员启用禁用
  async status(){
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.request.body)||app.validator.validate({status:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    if(ctx.request.body.id==1){
      return ctx.helper.resError('不能修改超级管理员');
    }
    let result = await ctx.service.admin.admin.status(ctx.request.body);
    if(result[0]){
      return ctx.helper.resSuccess(true);
    }else{
      return ctx.helper.resError('更新失败');
    }
  }

  // 管理员修改密码
  async editPassword(){
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    if(app.validator.validate({password:'string'},ctx.request.body)){
      return ctx.helper.resError('新密码不能为空');
    }
    if(app.validator.validate({password2:'string'},ctx.request.body)){
      return ctx.helper.resError('确认密码不能为空');
    }
    if(ctx.request.body.password!=ctx.request.body.password2){
      return ctx.helper.resError('确认密码不一致');
    }
    if(ctx.request.body.id==1){
      return ctx.helper.resError('不能修改超级管理员');
    }
    ctx.request.body.password = crypto.createHash('md5').update(`${app.config.crypto.secret}${ctx.request.body.password}`).digest('hex');
    let result = await ctx.service.admin.admin.editPassword(ctx.request.body);
    if(result[0]){
      return ctx.helper.resSuccess(true);
    }else{
      return ctx.helper.resError('更新失败');
    }
  }

  // 管理员模块获取角色列表
  async roleList() {
    const { ctx } = this;
    if(ctx.query.pageNO){
      let {count,rows} = await ctx.service.admin.admin.roleList(ctx.query);
      return ctx.helper.resSuccess({
        total:count,
        data:rows
      });
    }else{
      let result = await ctx.service.admin.admin.roleList(ctx.query);
      return ctx.helper.resSuccess(result);
    }
  }
}

module.exports = AdminController;
