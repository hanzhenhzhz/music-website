'use strict';

const Controller = require('egg').Controller;

class ClassifyController extends Controller {
  // 音乐分类列表
  async page() {
    const { ctx } = this;
    let {count,rows} = await ctx.service.admin.classify.page(ctx.query);
    return ctx.helper.resSuccess({
      total:count,
      data:rows
    });
  }

  // 音乐分类详情
  async detail() {
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.query)){
      return ctx.helper.resError('请求参数有误');
    }
    let detail = await ctx.service.admin.classify.detail(ctx.query);
    if(detail){
      return ctx.helper.resSuccess(detail);
    }else{
      return ctx.helper.resError('获取信息失败');
    }
  }

  // 音乐分类添加/编辑
  async form() {
    const { app,ctx } = this;
    if(app.validator.validate({name:'string'},ctx.request.body)){
      return ctx.helper.resError('分类名不能为空');
    }
    if(app.validator.validate({status:'int'},ctx.request.body)){
        return ctx.helper.resError('状态不能为空');
    }
    if(ctx.request.body.id){
      let result = await ctx.service.admin.classify.edit(ctx.request.body);
      if(result[0]){
        return ctx.helper.resSuccess(true);
      }else{
        return ctx.helper.resError('更新失败');
      }
    }else{
      let result = await ctx.service.admin.classify.add(ctx.request.body);
      if(result.id){
        return ctx.helper.resSuccess(result.id);
      }else{
        return ctx.helper.resError('添加失败');
      }
    }
  }

  // 音乐分类删除
  async del() {
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let result = await ctx.service.admin.classify.del(ctx.request.body);
    if(result[0]){
      return ctx.helper.resSuccess(true);
    }else{
      return ctx.helper.resError('删除失败');
    }
  }

  // 音乐分类状态修改
  async status(){
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.request.body)||app.validator.validate({status:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let result = await ctx.service.admin.classify.status(ctx.request.body);
    if(result[0]){
      return ctx.helper.resSuccess(true);
    }else{
      return ctx.helper.resError('更新失败');
    }
  }
}

module.exports = ClassifyController;
