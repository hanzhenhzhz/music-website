'use strict';

const Sequelize = require('sequelize');
const Controller = require('egg').Controller;

class MusicController extends Controller {
  // 歌曲列表
  async page() {
    const { ctx } = this;
    let {count,rows} = await ctx.service.user.music.page(ctx.query);
    for(let i of rows){
      if(i.dataValues.isCollection){
        i.dataValues.isCollection = true;
      }else{
        i.dataValues.isCollection = false;
      }
      if(i.dataValues.isLikes){
        i.dataValues.isLikes = true;
      }else{
        i.dataValues.isLikes = false;
      }
    }
    return ctx.helper.resSuccess({
      total:count,
      data:rows
    });
  }

  // 歌曲详情
  async detail() {
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.query)){
      return ctx.helper.resError('请求参数有误');
    }
    let detail = await ctx.service.user.music.detail(ctx.query);
    if(detail){
      if(detail.dataValues.isCollection){
        detail.dataValues.isCollection = true;
      }else{
        detail.dataValues.isCollection = false;
      }
      if(detail.dataValues.isLikes){
        detail.dataValues.isLikes = true;
      }else{
        detail.dataValues.isLikes = false;
      }
      return ctx.helper.resSuccess(detail);
    }else{
      return ctx.helper.resSuccess(null);
    }
  }

  // 歌曲播放量加1
  async addPlayNum() {
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let result = await ctx.service.user.music.addPlayNum(ctx.request.body);
    if(result.changedRows){
      return ctx.helper.resSuccess(true);
    }else{
      return ctx.helper.resSuccess(false);
    }
  }

  // 歌曲评论列表
  async commentPage() {
    const { app,ctx } = this;
    if(app.validator.validate({cid:'int'},ctx.query)){
      return ctx.helper.resError('请求参数有误');
    }
    let {count,rows} = await ctx.service.user.music.commentPage(ctx.query);
    for(let i of rows){
      if(i.dataValues.isLikes){
        i.dataValues.isLikes = true;
      }else{
        i.dataValues.isLikes = false;
      }
    }
    return ctx.helper.resSuccess({
      total:count,
      data:rows
    });
  }

  // 歌曲评论/回复
  async comment() {
    const { app,ctx } = this;
    if(app.validator.validate({cid:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    if(app.validator.validate({content:'string'},ctx.request.body)){
      return ctx.helper.resError('评论内容不能为空');
    }
    let result = await ctx.service.user.music.comment(ctx.request.body);
    if(result.id){
      return ctx.helper.resSuccess(result.id);
    }else{
      return ctx.helper.resError('评论失败');
    }
  }

  // 歌曲评论点赞
  async doCommentLikes() {
    const { app,ctx } = this;
    if(app.validator.validate({lid:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let detail = await ctx.service.user.music.isCommentLikes(ctx.request.body);
    if(detail){
      return ctx.helper.resError('已经点过赞了');
    }
    let result = await ctx.service.user.music.doCommentLikes(ctx.request.body);
    if(result.id){
      return ctx.helper.resSuccess('点赞成功');
    }else{
      return ctx.helper.resError('点赞失败');
    }
  }

  // 歌曲评论取消点赞
  async doClearCommentLikes() {
    const { app,ctx } = this;
    if(app.validator.validate({lid:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let result = await ctx.service.user.music.doClearCommentLikes(ctx.request.body);
    if(result){
      return ctx.helper.resSuccess('取消成功');
    }else{
      return ctx.helper.resError('取消点赞失败');
    }
  }

  // 歌曲点赞
  async doLikes() {
    const { app,ctx } = this;
    if(app.validator.validate({lid:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let detail = await ctx.service.user.music.isLikes(ctx.request.body);
    if(detail){
      return ctx.helper.resError('已经点过赞了');
    }
    let result = await ctx.service.user.music.doLikes(ctx.request.body);
    if(result.id){
      return ctx.helper.resSuccess('点赞成功');
    }else{
      return ctx.helper.resError('点赞失败');
    }
  }

  // 歌曲取消点赞
  async doClearLikes() {
    const { app,ctx } = this;
    if(app.validator.validate({lid:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let result = await ctx.service.user.music.doClearLikes(ctx.request.body);
    if(result){
      return ctx.helper.resSuccess('取消成功');
    }else{
      return ctx.helper.resError('取消点赞失败');
    }
  }

  // 歌曲收藏
  async doCollection() {
    const { app,ctx } = this;
    if(app.validator.validate({cid:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let detail = await ctx.service.user.music.isCollection(ctx.request.body);
    if(detail){
      return ctx.helper.resError('已经收藏过了');
    }
    let result = await ctx.service.user.music.doCollection(ctx.request.body);
    if(result.id){
      return ctx.helper.resSuccess('收藏成功');
    }else{
      return ctx.helper.resError('收藏失败');
    }
  }

  // 歌曲取消收藏
  async doClearCollection() {
    const { app,ctx } = this;
    if(app.validator.validate({cid:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let result = await ctx.service.user.music.doClearCollection(ctx.request.body);
    if(result){
      return ctx.helper.resSuccess('取消成功');
    }else{
      return ctx.helper.resError('取消收藏失败');
    }
  }

  // 收藏的歌曲
  async collectionPage() {
    const { ctx } = this;
    let {count,rows} = await ctx.service.user.music.collectionPage(ctx.query);
    for(let i of rows){
      if(i.dataValues.isCollection){
        i.dataValues.isCollection = true;
      }else{
        i.dataValues.isCollection = false;
      }
      if(i.dataValues.isLikes){
        i.dataValues.isLikes = true;
      }else{
        i.dataValues.isLikes = false;
      }
    }
    return ctx.helper.resSuccess({
      total:count,
      data:rows
    });
  }
}

module.exports = MusicController;
