'use strict';

const Controller = require('egg').Controller;
class RecommendController extends Controller {
  // 首页推荐
  async recommend(){
    const { ctx } = this;
    let topMusic = await ctx.service.user.music.getTopMusic();
    let topPlaylist = await ctx.service.user.playlist.getTopPlaylist();
    let albumPlayNum = await ctx.service.user.album.getAlbumPlayNum();
    albumPlayNum.sort((a,b)=>{return b.playNum-a.playNum});
    let topAlbum = albumPlayNum.slice(0,10);
    let starPlayNum = await ctx.service.user.star.getStarPlayNum();
    starPlayNum.sort((a,b)=>{return b.playNum-a.playNum});
    let topStar = starPlayNum.slice(0,10);
    for(let i of topMusic){
      if(i.dataValues.isCollection){
        i.dataValues.isCollection = true;
      }else{
        i.dataValues.isCollection = false;
      }
      if(i.dataValues.isLikes){
        i.dataValues.isLikes = true;
      }else{
        i.dataValues.isLikes = false;
      }
    }
    for(let i of topPlaylist){
      if(i.dataValues.isCollection){
        i.dataValues.isCollection = true;
      }else{
        i.dataValues.isCollection = false;
      }
      if(i.dataValues.isLikes){
        i.dataValues.isLikes = true;
      }else{
        i.dataValues.isLikes = false;
      }
      try{
        i.dataValues.musicNum = JSON.parse(i.dataValues.musics).length;
      }catch(e){
        i.dataValues.musicNum = 0;
      }
      delete i.dataValues.musics;
    }
    for(let i of topAlbum){
      if(i.dataValues.isCollection){
        i.dataValues.isCollection = true;
      }else{
        i.dataValues.isCollection = false;
      }
      if(i.dataValues.isLikes){
        i.dataValues.isLikes = true;
      }else{
        i.dataValues.isLikes = false;
      }
    }
    for(let i of topStar){
      if(i.dataValues.isCollection){
        i.dataValues.isCollection = true;
      }else{
        i.dataValues.isCollection = false;
      }
      if(i.dataValues.isLikes){
        i.dataValues.isLikes = true;
      }else{
        i.dataValues.isLikes = false;
      }
    }
    return ctx.helper.resSuccess({
      topMusic,
      topPlaylist,
      topAlbum,
      topStar
    });
  }
}

module.exports = RecommendController;
