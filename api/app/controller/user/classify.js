'use strict';

const Controller = require('egg').Controller;
class ClassifyController extends Controller {
  // 分类列表
  async list(){
    const { ctx } = this;
    let result = await ctx.service.user.classify.list();
    return ctx.helper.resSuccess(result);
  }
}

module.exports = ClassifyController;
