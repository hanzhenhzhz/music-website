'use strict';

const Controller = require('egg').Controller;
class BannerController extends Controller {
  // 获取广告
  async getBanner(){
    const { app,ctx } = this;
    if(app.validator.validate({identity:'string'},ctx.query)){
      return ctx.helper.resError('广告唯一标识不能为空');
    }
    let detail = await ctx.service.user.banner.getBanner(ctx.query);
    if(detail){
      return ctx.helper.resSuccess(detail);
    }else{
      return ctx.helper.resSuccess(null);
    }
  }
}

module.exports = BannerController;
