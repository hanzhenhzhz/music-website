'use strict';

const Sequelize = require('sequelize');
const Controller = require('egg').Controller;

class StarController extends Controller {
  // 歌星列表
  async page() {
    const { ctx } = this;
    let result = await ctx.service.user.star.page(ctx.query);
    result.sort((a,b)=>{return b.playNum-a.playNum});
    let offset = 1;
    let limit = 10;
    let count = result.length;
    if(ctx.query.pageNO){
      offset = ctx.query.pageNO;
    }
    if(ctx.query.pageSize){
      limit = ctx.query.pageSize;
    }
    let rows = result.slice((offset-1)*limit,limit);
    for(let i of rows){
      if(i.dataValues.isCollection){
        i.dataValues.isCollection = true;
      }else{
        i.dataValues.isCollection = false;
      }
      if(i.dataValues.isLikes){
        i.dataValues.isLikes = true;
      }else{
        i.dataValues.isLikes = false;
      }
    }
    return ctx.helper.resSuccess({
      total:count,
      data:rows
    });
  }

  // 歌星详情
  async detail() {
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.query)){
      return ctx.helper.resError('请求参数有误');
    }
    let detail = await ctx.service.user.star.detail(ctx.query);
    if(detail){
      if(detail.dataValues.isCollection){
        detail.dataValues.isCollection = true;
      }else{
        detail.dataValues.isCollection = false;
      }
      if(detail.dataValues.isLikes){
        detail.dataValues.isLikes = true;
      }else{
        detail.dataValues.isLikes = false;
      }
      return ctx.helper.resSuccess(detail);
    }else{
      return ctx.helper.resSuccess(null);
    }
  }

  // 歌星评论列表
  async commentPage() {
    const { app,ctx } = this;
    if(app.validator.validate({cid:'int'},ctx.query)){
      return ctx.helper.resError('请求参数有误');
    }
    let {count,rows} = await ctx.service.user.star.commentPage(ctx.query);
    for(let i of rows){
      if(i.dataValues.isLikes){
        i.dataValues.isLikes = true;
      }else{
        i.dataValues.isLikes = false;
      }
    }
    return ctx.helper.resSuccess({
      total:count,
      data:rows
    });
  }

  // 歌星评论/回复
  async comment() {
    const { app,ctx } = this;
    if(app.validator.validate({cid:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    if(app.validator.validate({content:'string'},ctx.request.body)){
      return ctx.helper.resError('评论内容不能为空');
    }
    let result = await ctx.service.user.star.comment(ctx.request.body);
    if(result.id){
      return ctx.helper.resSuccess(result.id);
    }else{
      return ctx.helper.resError('评论失败');
    }
  }

  // 歌星评论点赞
  async doCommentLikes() {
    const { app,ctx } = this;
    if(app.validator.validate({lid:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let detail = await ctx.service.user.star.isCommentLikes(ctx.request.body);
    if(detail){
      return ctx.helper.resError('已经点过赞了');
    }
    let result = await ctx.service.user.star.doCommentLikes(ctx.request.body);
    if(result.id){
      return ctx.helper.resSuccess('点赞成功');
    }else{
      return ctx.helper.resError('点赞失败');
    }
  }

  // 歌星评论取消点赞
  async doClearCommentLikes() {
    const { app,ctx } = this;
    if(app.validator.validate({lid:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let result = await ctx.service.user.star.doClearCommentLikes(ctx.request.body);
    if(result){
      return ctx.helper.resSuccess('取消成功');
    }else{
      return ctx.helper.resError('取消点赞失败');
    }
  }

  // 歌星点赞
  async doLikes() {
    const { app,ctx } = this;
    if(app.validator.validate({lid:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let detail = await ctx.service.user.star.isLikes(ctx.request.body);
    if(detail){
      return ctx.helper.resError('已经点过赞了');
    }
    let result = await ctx.service.user.star.doLikes(ctx.request.body);
    if(result.id){
      return ctx.helper.resSuccess('点赞成功');
    }else{
      return ctx.helper.resError('点赞失败');
    }
  }

  // 歌星取消点赞
  async doClearLikes() {
    const { app,ctx } = this;
    if(app.validator.validate({lid:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let result = await ctx.service.user.star.doClearLikes(ctx.request.body);
    if(result){
      return ctx.helper.resSuccess('取消成功');
    }else{
      return ctx.helper.resError('取消点赞失败');
    }
  }

  // 歌星收藏
  async doCollection() {
    const { app,ctx } = this;
    if(app.validator.validate({cid:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let detail = await ctx.service.user.star.isCollection(ctx.request.body);
    if(detail){
      return ctx.helper.resError('已经收藏过了');
    }
    let result = await ctx.service.user.star.doCollection(ctx.request.body);
    if(result.id){
      return ctx.helper.resSuccess('收藏成功');
    }else{
      return ctx.helper.resError('收藏失败');
    }
  }

  // 歌星取消收藏
  async doClearCollection() {
    const { app,ctx } = this;
    if(app.validator.validate({cid:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let result = await ctx.service.user.star.doClearCollection(ctx.request.body);
    if(result){
      return ctx.helper.resSuccess('取消成功');
    }else{
      return ctx.helper.resError('取消收藏失败');
    }
  }

  // 收藏的歌星
  async collectionPage() {
    const { ctx } = this;
    let {count,rows} = await ctx.service.user.star.collectionPage(ctx.query);
    for(let i of rows){
      if(i.dataValues.isCollection){
        i.dataValues.isCollection = true;
      }else{
        i.dataValues.isCollection = false;
      }
      if(i.dataValues.isLikes){
        i.dataValues.isLikes = true;
      }else{
        i.dataValues.isLikes = false;
      }
    }
    return ctx.helper.resSuccess({
      total:count,
      data:rows
    });
  }
}

module.exports = StarController;
