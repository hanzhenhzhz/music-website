'use strict';

const Sequelize = require('sequelize');
const Controller = require('egg').Controller;

class PlaylistController extends Controller {
  // 歌单列表
  async page() {
    const { ctx } = this;
    let {count,rows} = await ctx.service.user.playlist.page(ctx.query);
    for(let i of rows){
      if(i.dataValues.isCollection){
        i.dataValues.isCollection = true;
      }else{
        i.dataValues.isCollection = false;
      }
      if(i.dataValues.isLikes){
        i.dataValues.isLikes = true;
      }else{
        i.dataValues.isLikes = false;
      }
      try{
        i.dataValues.musicNum = JSON.parse(i.dataValues.musics).length;
      }catch(e){
        i.dataValues.musicNum = 0;
      }
      delete i.dataValues.musics;
    }
    return ctx.helper.resSuccess({
      total:count,
      data:rows
    });
  }

  // 歌单详情
  async detail() {
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.query)){
      return ctx.helper.resError('请求参数有误');
    }
    let detail = await ctx.service.user.playlist.detail(ctx.query);
    if(detail){
      if(detail.dataValues.isCollection){
        detail.dataValues.isCollection = true;
      }else{
        detail.dataValues.isCollection = false;
      }
      if(detail.dataValues.isLikes){
        detail.dataValues.isLikes = true;
      }else{
        detail.dataValues.isLikes = false;
      }
      try{
        detail.dataValues.musicNum = JSON.parse(detail.dataValues.musics).length;
      }catch(e){
        detail.dataValues.musicNum = 0;
      }
      delete detail.dataValues.musics;
      return ctx.helper.resSuccess(detail);
    }else{
      return ctx.helper.resSuccess(null);
    }
  }

  // 歌单播放量加1
  async addPlayNum() {
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let result = await ctx.service.user.playlist.addPlayNum(ctx.request.body);
    if(result.changedRows){
      return ctx.helper.resSuccess(true);
    }else{
      return ctx.helper.resSuccess(false);
    }
  }

  // 歌单评论列表
  async commentPage() {
    const { app,ctx } = this;
    if(app.validator.validate({cid:'int'},ctx.query)){
      return ctx.helper.resError('请求参数有误');
    }
    let {count,rows} = await ctx.service.user.playlist.commentPage(ctx.query);
    for(let i of rows){
      if(i.dataValues.isLikes){
        i.dataValues.isLikes = true;
      }else{
        i.dataValues.isLikes = false;
      }
    }
    return ctx.helper.resSuccess({
      total:count,
      data:rows
    });
  }

  // 歌单评论/回复
  async comment() {
    const { app,ctx } = this;
    if(app.validator.validate({cid:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    if(app.validator.validate({content:'string'},ctx.request.body)){
      return ctx.helper.resError('评论内容不能为空');
    }
    let result = await ctx.service.user.playlist.comment(ctx.request.body);
    if(result.id){
      return ctx.helper.resSuccess(result.id);
    }else{
      return ctx.helper.resError('评论失败');
    }
  }

  // 歌单评论点赞
  async doCommentLikes() {
    const { app,ctx } = this;
    if(app.validator.validate({lid:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let detail = await ctx.service.user.playlist.isCommentLikes(ctx.request.body);
    if(detail){
      return ctx.helper.resError('已经点过赞了');
    }
    let result = await ctx.service.user.playlist.doCommentLikes(ctx.request.body);
    if(result.id){
      return ctx.helper.resSuccess('点赞成功');
    }else{
      return ctx.helper.resError('点赞失败');
    }
  }

  // 歌单评论取消点赞
  async doClearCommentLikes() {
    const { app,ctx } = this;
    if(app.validator.validate({lid:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let result = await ctx.service.user.playlist.doClearCommentLikes(ctx.request.body);
    if(result){
      return ctx.helper.resSuccess('取消成功');
    }else{
      return ctx.helper.resError('取消点赞失败');
    }
  }

  // 歌单点赞
  async doLikes() {
    const { app,ctx } = this;
    if(app.validator.validate({lid:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let detail = await ctx.service.user.playlist.isLikes(ctx.request.body);
    if(detail){
      return ctx.helper.resError('已经点过赞了');
    }
    let result = await ctx.service.user.playlist.doLikes(ctx.request.body);
    if(result.id){
      return ctx.helper.resSuccess('点赞成功');
    }else{
      return ctx.helper.resError('点赞失败');
    }
  }

  // 歌单取消点赞
  async doClearLikes() {
    const { app,ctx } = this;
    if(app.validator.validate({lid:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let result = await ctx.service.user.playlist.doClearLikes(ctx.request.body);
    if(result){
      return ctx.helper.resSuccess('取消成功');
    }else{
      return ctx.helper.resError('取消点赞失败');
    }
  }

  // 歌单收藏
  async doCollection() {
    const { app,ctx } = this;
    if(app.validator.validate({cid:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let detail = await ctx.service.user.playlist.isCollection(ctx.request.body);
    if(detail){
      return ctx.helper.resError('已经收藏过了');
    }
    let result = await ctx.service.user.playlist.doCollection(ctx.request.body);
    if(result.id){
      return ctx.helper.resSuccess('收藏成功');
    }else{
      return ctx.helper.resError('收藏失败');
    }
  }

  // 歌单取消收藏
  async doClearCollection() {
    const { app,ctx } = this;
    if(app.validator.validate({cid:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let result = await ctx.service.user.playlist.doClearCollection(ctx.request.body);
    if(result){
      return ctx.helper.resSuccess('取消成功');
    }else{
      return ctx.helper.resError('取消收藏失败');
    }
  }

  // 收藏的歌单
  async collectionPage() {
    const { ctx } = this;
    let {count,rows} = await ctx.service.user.playlist.collectionPage(ctx.query);
    for(let i of rows){
      if(i.dataValues.isCollection){
        i.dataValues.isCollection = true;
      }else{
        i.dataValues.isCollection = false;
      }
      if(i.dataValues.isLikes){
        i.dataValues.isLikes = true;
      }else{
        i.dataValues.isLikes = false;
      }
      try{
        i.dataValues.musicNum = JSON.parse(i.dataValues.musics).length;
      }catch(e){
        i.dataValues.musicNum = 0;
      }
      delete i.dataValues.musics;
    }
    return ctx.helper.resSuccess({
      total:count,
      data:rows
    });
  }

  // 歌单下歌曲列表
  async musicList() {
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.query)){
      return ctx.helper.resError('请求参数有误');
    }
    let result = await ctx.service.user.playlist.musicList(ctx.query);
    if(result){
        let {count,rows} = result;
        for(let i of rows){
            if(i.dataValues.isCollection){
              i.dataValues.isCollection = true;
            }else{
              i.dataValues.isCollection = false;
            }
            if(i.dataValues.isLikes){
              i.dataValues.isLikes = true;
            }else{
              i.dataValues.isLikes = false;
            }
        }
        return ctx.helper.resSuccess({
          total:count,
          data:rows
        });
    }else{
        return ctx.helper.resSuccess({
            total:0,
            data:[]
        });
    }
  }

  // 我的歌单列表
  async myPage() {
    const { ctx } = this;
    let {count,rows} = await ctx.service.user.playlist.myPage(ctx.query);
    for(let i of rows){
      try{
        i.dataValues.musicNum = JSON.parse(i.dataValues.musics).length;
      }catch(e){
        i.dataValues.musicNum = 0;
      }
      delete i.dataValues.musics;
    }
    return ctx.helper.resSuccess({
      total:count,
      data:rows
    });
  }
  
  // 我的歌单详情
  async myDetail() {
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.query)){
      return ctx.helper.resError('请求参数有误');
    }
    let detail = await ctx.service.user.playlist.myDetail(ctx.query);
    if(detail){
      try{
        detail.dataValues.musicNum = JSON.parse(detail.dataValues.musics).length;
      }catch(e){
        detail.dataValues.musicNum = 0;
      }
      delete detail.dataValues.musics;
      return ctx.helper.resSuccess(detail);
    }else{
      return ctx.helper.resSuccess(null);
    }
  }
  
  // 歌单添加/编辑
  async form() {
    const { app,ctx } = this;
    if(app.validator.validate({name:'string'},ctx.request.body)){
      return ctx.helper.resError('歌单名称不能为空');
    }
    if(app.validator.validate({private:'int'},ctx.request.body)){
      return ctx.helper.resError('是否私有不能为空');
    }
    if(ctx.request.body.id){
      let result = await ctx.service.user.playlist.edit(ctx.request.body);
      if(result[0]){
        return ctx.helper.resSuccess(true);
      }else{
        return ctx.helper.resError('更新失败');
      }
    }else{
      let result = await ctx.service.user.playlist.add(ctx.request.body);
      if(result.id){
        return ctx.helper.resSuccess(result.id);
      }else{
        return ctx.helper.resError('添加失败');
      }
    }
  }
  
  // 歌单删除
  async del() {
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let result = await ctx.service.user.playlist.del(ctx.request.body);
    if(result[0]){
      return ctx.helper.resSuccess(true);
    }else{
      return ctx.helper.resError('删除失败');
    }
  }
  
  // 歌单歌曲添加
  async addMusic() {
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.request.body)&&app.validator.validate({mid:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let result = await ctx.service.user.playlist.addMusic(ctx.request.body);
    if(result[0]){
      return ctx.helper.resSuccess(true);
    }else{
      return ctx.helper.resError('添加失败');
    }
  }
  
  // 歌单歌曲删除
  async delMusic() {
    const { app,ctx } = this;
    if(app.validator.validate({id:'int'},ctx.request.body)&&app.validator.validate({mid:'int'},ctx.request.body)){
      return ctx.helper.resError('请求参数有误');
    }
    let result = await ctx.service.user.playlist.delMusic(ctx.request.body);
    if(result[0]){
      return ctx.helper.resSuccess(true);
    }else{
      return ctx.helper.resError('删除失败');
    }
  }
  
}

module.exports = PlaylistController;
