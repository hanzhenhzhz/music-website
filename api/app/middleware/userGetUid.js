module.exports = (options,app) => {
    return async function userGetUid(ctx, next) {
      try{
        let {id,utype} = ctx.app.jwt.verify(ctx.request.header.token,ctx.app.config.jwt.secret);
        if(!utype||utype!='user'){
          throw new Error('error');
        }
        ctx.request.header.uid = id;
        await next();
      }catch(e){
        if(e.name=='JsonWebTokenError'||e.name=='TokenExpiredError'){
          await next();
        }else{
          ctx.status = 500;
          ctx.body = {
            message: '系统错误',
            error:e
          };
        }
      }
    };
};
