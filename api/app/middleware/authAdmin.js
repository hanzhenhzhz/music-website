module.exports = (options,app) => {
  return async function authAdmin(ctx, next) {
    try{
      let {id,rid,utype} = ctx.app.jwt.verify(ctx.request.header.token,ctx.app.config.jwt.secret);
      if(!utype||utype!='admin'){
        throw new Error('error');
      }
      if(rid==1){
        ctx.request.header.uid = id;
        ctx.request.header.rid = rid;
        await next();
      }else{
        let menuList = await ctx.service.admin.role.roleMenuList({id:rid});
        let hasMenu = false;
        for(let i of menuList){
          if(ctx.request.url==i.api){
            hasMenu = true;
            break;
          }
        }
        if(hasMenu){
          ctx.request.header.uid = id;
          ctx.request.header.rid = rid;
          await next();
        }else{
          ctx.status = 401;
          ctx.body = {
            message: '没有权限'
          };
        }
      }
    }catch(e){
      if(e.name=='JsonWebTokenError'||e.name=='TokenExpiredError'){
        ctx.status = 401;
        ctx.body = {
          message: '无效token'
        };
      }else{
        ctx.status = 500;
        ctx.body = {
          message: '系统错误',
          error:e
        };
      }
    }
  };
};
