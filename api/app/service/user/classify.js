'use strict';

const Service = require('egg').Service;
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

class ClassifyService extends Service {
    // 分类列表
    async list(query) {
      const { ctx } = this;
      let config = {
        where:{
          isDel:0,
          status:1
        },
        order:[
          ['sort', 'DESC']
        ],
        attributes: { 
          exclude: ['isDel','delTime','status','createTime','updateTime','sort'],          
        }
      };
      return await ctx.model.Classify.findAll(config);
    }
  }

  module.exports = ClassifyService;
