'use strict';

const Service = require('egg').Service;
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

class BannerService extends Service {
  // 获取广告
  async getBanner(query) {
    const { ctx } = this;
    let config = {
      where:{
        isDel:0,
        status:1,
        identity:query.identity
      },
      attributes: {
        exclude: ['status','isDel','delTime','createTime','updateTime']
      }
    };
    return await ctx.model.Banner.findOne(config);
  }
}

module.exports = BannerService;
