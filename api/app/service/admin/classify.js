'use strict';

const Service = require('egg').Service;
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

class ClassifyService extends Service {
    // 音乐分类列表
    async page(query) {
      const { ctx } = this;
      let config = {
        where:{
          isDel:0
        },
        order:[
          ['createTime', 'DESC']
        ],
        attributes: { 
          exclude: ['isDel','delTime'],          
        }
      };
      if(query.name){
        config.where.name = {
          [Op.like]:`%${query.name}%`
        }
      }
      if(!query.pageNO){
        query.pageNO = 1;
      }
      if(!query.pageSize){
        query.pageSize = 10;
      }
      config.offset = (query.pageNO-1)*query.pageSize;
      config.limit = query.pageSize;
      return await ctx.model.Classify.findAndCountAll(config);
    }

    // 音乐分类详情
    async detail(query) {
      const { ctx } = this;
      let config = {
        where:{
          isDel:0,
          id:query.id
        },
        attributes: { 
          exclude: ['isDel','delTime'] 
        }
      };
      return await ctx.model.Classify.findOne(config);
    }

    // 音乐分类添加
    async add(data) {
      const { ctx } = this;
      return await ctx.model.Classify.create({
        name:data.name,
        sort:data.sort?data.sort:0,
        desc:data.desc?data.desc:'',
        status:data.status,
        cover:data.cover?data.cover:'',
        createTime:new Date().getTime(),
        updateTime:new Date().getTime(),
        delTime:0
      });
    }

    // 音乐分类编辑
    async edit(data) {
      const { ctx } = this;
      return await ctx.model.Classify.update({
        name:data.name,
        sort:data.sort?data.sort:0,
        desc:data.desc?data.desc:'',
        status:data.status,
        cover:data.cover?data.cover:'',
        updateTime:new Date().getTime()
      },{
        where:{
          id:data.id
        }
      });
    }

    // 音乐分类删除
    async del(data) {
      const { ctx } = this;
      return await ctx.model.Classify.update({
        isDel:1,
        delTime:new Date().getTime()
      },{
        where:{
          id:data.id
        }
      });
    }

    // 音乐分类修改状态
    async status(data) {
      const { ctx } = this;
      return await ctx.model.Classify.update({
        status:data.status,
        updateTime:new Date().getTime()
      },{
        where:{
          id:data.id
        }
      });
    }
  }

  module.exports = ClassifyService;
