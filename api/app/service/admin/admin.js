'use strict';

const Service = require('egg').Service;
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

class AdminService extends Service {
  // 管理员列表
  async page(query) {
    const { ctx } = this;
    let config = {
      where:{
        isDel:0
      },
      order:[
        ['createTime', 'DESC']
      ],
      attributes: {
        include: [[Sequelize.col('role.name'), 'roleName']],
        exclude: ['password','isDel','delTime'],
      },
      include: [{
        model: ctx.model.Role,
        as: 'role',
        where: {
          isDel:0
        },
        duplicating:false,
        required:false,
        attributes: []
      }],
      distinct: true
    };
    if(query.account){
      config.where.account = query.account;
    }
    if(query.nickname){
      config.where.nickname = {
        [Op.like]:`%${query.nickname}%`
      }
    }
    if(query.rid){
      config.where.rid = query.rid;
    }
    if(!query.pageNO){
      query.pageNO = 1;
    }
    if(!query.pageSize){
      query.pageSize = 10;
    }
    config.offset = (query.pageNO-1)*query.pageSize;
    config.limit = query.pageSize;
    return await ctx.model.Admin.findAndCountAll(config);
  }

  // 管理员详情
  async detail(query) {
    const { ctx } = this;
    let config = {
      where:{
        isDel:0,
        id:query.id
      },
      attributes: {
        include: [[Sequelize.col('role.name'), 'roleName']],
        exclude: ['password','isDel','delTime']
      },
      include: [{
        model: ctx.model.Role,
        as: 'role',
        where: {
          isDel:0
        },
        duplicating:false,
        required:false,
        attributes: []
      }]
    };
    return await ctx.model.Admin.findOne(config);
  }

  // 管理员添加
  async add(data) {
    const { ctx } = this;
    return await ctx.model.Admin.create({
      account:data.account,
      password:data.password,
      nickname:data.nickname,
      headimg:data.headimg?data.headimg:'',
      rid:data.rid,
      status:data.status,
      createTime:new Date().getTime(),
      updateTime:new Date().getTime(),
      delTime:0
    });
  }

  // 管理员编辑
  async edit(data) {
    const { ctx } = this;
    return await ctx.model.Admin.update({
      account:data.account,
      nickname:data.nickname,
      headimg:data.headimg?data.headimg:'',
      rid:data.rid,
      status:data.status,
      updateTime:new Date().getTime()
    },{
      where:{
        id:data.id
      }
    });
  }

  // 管理员删除
  async del(data) {
    const { ctx } = this;
    return await ctx.model.Admin.update({
      isDel:1,
      delTime:new Date().getTime()
    },{
      where:{
        id:data.id
      }
    });
  }

  // 管理员启用禁用
  async status(data) {
    const { ctx } = this;
    return await ctx.model.Admin.update({
      status:data.status,
      updateTime:new Date().getTime()
    },{
      where:{
        id:data.id
      }
    });
  }

  // 管理员修改密码
  async editPassword(data) {
    const { ctx } = this;
    return await ctx.model.Admin.update({
      password:data.password,
      updateTime:new Date().getTime()
    },{
      where:{
        id:data.id
      }
    });
  }

  // 管理员模块获取角色列表
  async roleList(query) {
    const { ctx } = this;
    if(ctx.pageNO){
      let config = {
        where:{
          isDel:0
        },
        order:[
          ['createTime', 'DESC']
        ],
        attributes: ['id', 'name', 'desc']
      };
      if(query.name){
        config.where.name = {
          [Op.like]:`%${query.name}%`
        }
      }
      if(!query.pageSize){
        query.pageSize = 10;
      }
      config.offset = (query.pageNO-1)*query.pageSize;
      config.limit = query.pageSize;
      return await ctx.model.Role.findAndCountAll(config);
    }else{
      let config = {
        where:{
          isDel:0
        },
        order:[
          ['createTime', 'DESC']
        ],
        attributes: ['id', 'name', 'desc']
      };
      if(query.name){
        config.where.name = {
          [Op.like]:`%${query.name}%`
        }
      }
      return await ctx.model.Role.findAll(config);
    }
  }

  // 根据账号获取管理员详情
  async getAccountdetail(query) {
    const { ctx } = this;
    let config = {
      where:{
        isDel:0,
        account:query.account
      },
      attributes: ['id']
    };
    return await ctx.model.Admin.findOne(config);
  }

  // 管理员登录
  async login(data) {
    const { ctx } = this;
    let config = {
      where:{
        isDel:0,
        account:data.account,
        password:data.password
      },
      attributes: ['id','rid','status']
    };
    return await ctx.model.Admin.findOne(config);
  }

  // 管理员修改自己信息
  async editInfo(data) {
    const { ctx } = this;
    return await ctx.model.Admin.update({
      account:data.account,
      nickname:data.nickname,
      headimg:data.headimg?data.headimg:'',
      updateTime:new Date().getTime()
    },{
      where:{
        id:data.id
      }
    });
  }
}

module.exports = AdminService;
