'use strict';

const Service = require('egg').Service;
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

class MenuService extends Service {
    // 模块集合
    async list(query) {
      const { ctx } = this;
      let config = {
        where:{
          isDel:0
        },
        attributes: { exclude: ['isDel','delTime'] }
      };
      if(query.identity){
        config.where.identity = query.identity;
      }
      if(query.name){
        config.where.name = {
          [Op.like]:`%${query.name}%`
        }
      }
      return await ctx.model.Menu.findAll(config);
    }

    // 模块详情
    async detail(query) {
      const { ctx } = this;
      let config = {
        where:{
          isDel:0,
          id:query.id
        },
        attributes: { exclude: ['isDel','delTime'] }
      };
      return await ctx.model.Menu.findOne(config);
    }

    // 模块添加
    async add(data) {
      const { ctx } = this;
      return await ctx.model.Menu.create({
        pid:data.pid,
        name:data.name,
        desc:data.desc?data.desc:'',
        api:data.api?data.api:'',
        identity:data.identity,
        createTime:new Date().getTime(),
        updateTime:new Date().getTime(),
        delTime:0
      });
    }

    // 模块编辑
    async edit(data) {
      const { ctx } = this;
      return await ctx.model.Menu.update({
        pid:data.pid,
        name:data.name,
        desc:data.desc?data.desc:'',
        api:data.api?data.api:'',
        identity:data.identity,
        updateTime:new Date().getTime(),
        delTime:0
      },{
        where:{
          id:data.id
        }
      });
    }

    // 递归获取模块集合
    async recursiveMenu(arr,id){
      const { ctx } = this;
      let config = {
        where:{
          isDel:0,
          pid:id
        },
        attributes: ['id']
      };
      let result = await ctx.model.Menu.findOne(config);
      if(result){
        arr.push(result.id);
        await this.recursiveMenu(arr,result.id);
      }
    }

    // 模块删除
    async del(data) {
      const { ctx } = this;
      let delArr = [data.id];
      await this.recursiveMenu(delArr,data.id);
      return await ctx.model.Menu.update({
        isDel:1,
        delTime:new Date().getTime()
      },{
        where:{
          id:{
            [Op.in]:delArr
          }
        }
      });
    }

    // 根据唯一标识获取模块详情
    async getIdentitydetail(query) {
      const { ctx } = this;
      let config = {
        where:{
          isDel:0,
          identity:query.identity
        },
        attributes: ['id']
      };
      return await ctx.model.Menu.findOne(config);
    }

  }

  module.exports = MenuService;
