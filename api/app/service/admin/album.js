'use strict';

const Service = require('egg').Service;
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

class AlbumService extends Service {
    // 专辑列表
    async page(query) {
      const { ctx } = this;
      let config = {
        where:{
          isDel:0
        },
        order:[
          ['createTime', 'DESC']
        ],
        attributes: {
          include: [
            [
              Sequelize.literal(`(
                  SELECT COUNT(*)
                  FROM collection
                  WHERE
                      collection.cid = album.id
                      AND
                      collection.type = 3
              )`),
              'collectionNum'
            ],
            [
              Sequelize.literal(`(
                  SELECT COUNT(*)
                  FROM likes
                  WHERE
                      likes.lid = album.id
                      AND
                      likes.type = 3
              )`),
              'likesNum'
            ],
            [
              Sequelize.literal(`(
                  SELECT COUNT(*)
                  FROM comment
                  WHERE
                      comment.cid = album.id
                      AND
                      comment.type = 3
                      AND
                      comment.isDel = 0
              )`),
              'commentNum'
            ],
            [Sequelize.col('star.name'), 'starName']
          ],
          exclude: ['isDel','delTime'],
        },
        include: [{
          model: ctx.model.Star,
          as: 'star',
          where: {
            isDel:0
          },
          duplicating:false,
          required:false,
          attributes: []
        }],
        distinct: true
      };
      if(query.name){
        config.where.name = {
          [Op.like]:`%${query.name}%`
        }
      }
      if(query.sid){
        config.where.sid = query.sid;
      }
      if(!query.pageNO){
        query.pageNO = 1;
      }
      if(!query.pageSize){
        query.pageSize = 10;
      }
      config.offset = (query.pageNO-1)*query.pageSize;
      config.limit = query.pageSize;
      return await ctx.model.Album.findAndCountAll(config);
    }

    // 专辑详情
    async detail(query) {
      const { ctx } = this;
      let config = {
        where:{
          isDel:0,
          id:query.id
        },
        attributes: {
          include: [
            [
              Sequelize.literal(`(
                  SELECT COUNT(*)
                  FROM collection
                  WHERE
                      collection.cid = album.id
                      AND
                      collection.type = 3
              )`),
              'collectionNum'
            ],
            [
              Sequelize.literal(`(
                  SELECT COUNT(*)
                  FROM likes
                  WHERE
                      likes.lid = album.id
                      AND
                      likes.type = 3
              )`),
              'likesNum'
            ],
            [
              Sequelize.literal(`(
                  SELECT COUNT(*)
                  FROM comment
                  WHERE
                      comment.cid = album.id
                      AND
                      comment.type = 3
                      AND
                      comment.isDel = 0
              )`),
              'commentNum'
            ],
            [Sequelize.col('star.name'), 'starName']
          ],
          exclude: ['isDel','delTime']
        },
        include: [{
          model: ctx.model.Star,
          as: 'star',
          where: {
            isDel:0
          },
          duplicating:false,
          required:false,
          attributes: []
        }]
      };
      return await ctx.model.Album.findOne(config);
    }

    // 专辑添加
    async add(data) {
      const { ctx } = this;
      return await ctx.model.Album.create({
        name:data.name,
        desc:data.desc?data.desc:'',
        cover:data.cover?data.cover:'',
        sid:data.sid,
        status:data.status,
        createTime:new Date().getTime(),
        updateTime:new Date().getTime(),
        delTime:0
      });
    }

    // 专辑编辑
    async edit(data) {
      const { ctx } = this;
      return await ctx.model.Album.update({
        name:data.name,
        desc:data.desc?data.desc:'',
        cover:data.cover?data.cover:'',
        sid:data.sid,
        status:data.status,
        updateTime:new Date().getTime()
      },{
        where:{
          id:data.id
        }
      });
    }

    // 专辑删除
    async del(data) {
      const { ctx } = this;
      const t = await ctx.model.transaction();
      try{
        let [collectionDel,likeDel,commentDel,albumDel] = await Promise.all(
          [
            // 删除收藏
            ctx.model.Collection.destroy({
              where: {
                type:3,
                cid:data.id
              },
              transaction:t
            }),
            // 删除点赞
            ctx.model.Likes.destroy({
              where: {
                type:3,
                lid:data.id
              },
              transaction:t
            }),
            // 删除评论
            ctx.model.Comment.update({
              isDel:1,
              delTime:new Date().getTime()
            },{
              where:{
                type:3,
                cid:data.id
              },
              transaction:t
            }),
            // 删除专辑
            ctx.model.Album.update({
              isDel:1,
              delTime:new Date().getTime()
            },{
              where:{
                id:data.id
              },
              transaction:t
            })
          ]
        );
        if(!albumDel[0]){
          throw new Error('error');
        }
        await t.commit();
        return albumDel;
      }catch(e){
        await t.rollback();
        return [0];
      }
    }

    // 专辑修改状态
    async status(data) {
      const { ctx } = this;
      return await ctx.model.Album.update({
        status:data.status,
        updateTime:new Date().getTime()
      },{
        where:{
          id:data.id
        }
      });
    }

    // 添加/编辑专辑获取歌星列表
    async starList(query) {
      const { ctx } = this;
      if(ctx.pageNO){
        let config = {
          where:{
            isDel:0
          },
          order:[
            ['createTime', 'DESC']
          ],
          attributes: ['id', 'name', 'desc']
        };
        if(query.name){
          config.where.name = {
            [Op.like]:`%${query.name}%`
          }
        }
        if(!query.pageSize){
          query.pageSize = 10;
        }
        config.offset = (query.pageNO-1)*query.pageSize;
        config.limit = query.pageSize;
        return await ctx.model.Star.findAndCountAll(config);
      }else{
        let config = {
          where:{
            isDel:0
          },
          order:[
            ['createTime', 'DESC']
          ],
          attributes: ['id', 'name', 'desc']
        };
        if(query.name){
          config.where.name = {
            [Op.like]:`%${query.name}%`
          }
        }
        return await ctx.model.Star.findAll(config);
      }
    }

    // 递归获取评论集合
    async recursiveComment(arr,id){
      const { ctx } = this;
      let config = {
        where:{
          isDel:0,
          pid:id,
          type:3
        },
        attributes: ['id']
      };
      let result = await ctx.model.Comment.findOne(config);
      if(result){
        arr.push(result.id);
        await this.recursiveComment(arr,result.id);
      }
    }

    // 专辑评论列表
    async commentPage(query) {
      const { ctx } = this;
      let config = {
        where:{
          isDel:0,
          cid:query.cid,
          type:3
        },
        order:[
          ['createTime', 'DESC']
        ],
        attributes: {
          include: [
            [
              Sequelize.literal(`(
                      SELECT COUNT(*)
                      FROM likes
                      WHERE
                          likes.lid = comment.id
                          AND
                          likes.type = 5
                  )`),
              'likesNum'
            ],
            [
              Sequelize.literal(`(
                      SELECT COUNT(*)
                      FROM comment AS c
                      WHERE
                          c.pid = comment.id
                          AND
                          c.type = 3
                          AND
                          c.isDel = 0
                  )`),
              'commentNum'
            ],
            [Sequelize.col('user.account'), 'account'],
            [Sequelize.col('user.nickname'), 'nickname']
          ],
          exclude: ['isDel','delTime','type'],
        },
        include: [{
          model: ctx.model.User,
          as: 'user',
          duplicating:false,
          required:false,
          attributes: []
        }],
        distinct: true
      };
      config.where.pid = query.pid?query.pid:0;
      if(!query.pageNO){
        query.pageNO = 1;
      }
      if(!query.pageSize){
        query.pageSize = 10;
      }
      config.offset = (query.pageNO-1)*query.pageSize;
      config.limit = query.pageSize;
      return await ctx.model.Comment.findAndCountAll(config);
    }

    // 专辑评论详情
    async commentDetail(query) {
      const { ctx } = this;
      let config = {
        where:{
          isDel:0,
          id:query.id,
          type:3
        },
        attributes: {
          include: [
            [
              Sequelize.literal(`(
                      SELECT COUNT(*)
                      FROM likes
                      WHERE
                          likes.lid = comment.id
                          AND
                          likes.type = 5
                  )`),
              'likesNum'
            ],
            [
              Sequelize.literal(`(
                      SELECT COUNT(*)
                      FROM comment AS c
                      WHERE
                          c.pid = comment.id
                          AND
                          comment.type = 3
                          AND
                          comment.isDel = 0
                  )`),
              'commentNum'
            ],
            [Sequelize.col('user.account'), 'account'],
            [Sequelize.col('user.nickname'), 'nickname']
          ],
          exclude: ['isDel','delTime','type']
        },
        include: [{
          model: ctx.model.User,
          as: 'user',
          duplicating:false,
          required:false,
          attributes: []
        }]
      };
      return await ctx.model.Comment.findOne(config);
    }

    // 专辑评论删除
    async commentDel(data) {
      const { ctx } = this;
      const t = await ctx.model.transaction();
      try{
        let delArr = [data.id];
        await this.recursiveComment(delArr,data.id);
        let [likeDel,commentDel] = await Promise.all(
          [
            // 删除点赞
            ctx.model.Likes.destroy({
              where: {
                type:5,
                lid:{
                  [Op.in]:delArr
                }
              },
              transaction:t
            }),
            // 删除评论
            ctx.model.Comment.update({
              isDel:1,
              delTime:new Date().getTime()
            },{
              where:{
                type:3,
                id:{
                  [Op.in]:delArr
                }
              },
              transaction:t
            }),
          ]
        );
        if(!commentDel[0]){
          throw new Error('error');
        }
        await t.commit();
        return commentDel;
      }catch(e){
        await t.rollback();
        return [0];
      }
    }

    // 专辑评论状态修改
    async commentStatus(data) {
      const { ctx } = this;
      return await ctx.model.Comment.update({
        status:data.status,
        updateTime:new Date().getTime()
      },{
        where:{
          id:data.id,
          type:3
        }
      });
    }

    // 获取专辑总数
    async getNum() {
      const { ctx } = this;
      return await ctx.model.Album.findOne({
        attributes: [
            [
              Sequelize.literal(`(
                  SELECT COUNT(*)
                  FROM album
                  WHERE
                      album.isDel = 0
              )`),
              'num'
            ]
          ]
      });
    }

    // 获取所有专辑播放量
    async getAlbumPlayNum(){
      const { ctx } = this;
      let config = {
        where:{
          isDel:0
        },
        attributes: {
          include: [
            [
              Sequelize.literal(`(
                    SELECT COUNT(*)
                    FROM collection
                    WHERE
                        collection.cid = album.id
                        AND
                        collection.type = 3
                )`),
              'collectionNum'
            ],
            [
              Sequelize.literal(`(
                    SELECT COUNT(*)
                    FROM likes
                    WHERE
                        likes.lid = album.id
                        AND
                        likes.type = 3
                )`),
              'likesNum'
            ],
            [
              Sequelize.literal(`(
                    SELECT COUNT(*)
                    FROM comment
                    WHERE
                        comment.cid = album.id
                        AND
                        comment.type = 3
                        AND
                        comment.isDel = 0
                )`),
              'commentNum'
            ],
            [
              Sequelize.literal(`(
                  SELECT SUM(music.playNum)
                  FROM music
                  WHERE
                      music.aid = album.id
                      AND
                      music.isDel = 0
              )`),
              'playNum'
            ]
          ],
          exclude: ['isDel','delTime'],
        }
      };
      return await ctx.model.Album.findAll(config);
    }
  }

  module.exports = AlbumService;
