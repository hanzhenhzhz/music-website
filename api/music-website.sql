/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : music-website

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 12/10/2021 11:12:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '账号',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `nickname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '昵称',
  `headimg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `rid` int(11) NOT NULL COMMENT '角色id',
  `isDel` int(11) NOT NULL COMMENT '是否已删除 1是 0否',
  `status` int(11) NOT NULL COMMENT '状态 1正常 0禁用',
  `createTime` bigint(13) NOT NULL COMMENT '创建时间',
  `updateTime` bigint(13) NOT NULL COMMENT '修改时间',
  `delTime` bigint(13) NOT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, 'admin', '7a184fc991737e2b4bf783526c35a587', '哈哈哈', '', 1, 0, 1, 1632809870541, 1633153541152, 0);
INSERT INTO `admin` VALUES (2, 'Jane1', '7a184fc991737e2b4bf783526c35a587', 'asdf', 'asdfas', 2, 0, 1, 1, 1632720639787, 1632721815021);
INSERT INTO `admin` VALUES (3, 'Jane', 'Doe', 'asdf', 'asdfas', 2, 1, 1, 1, 1, 1);
INSERT INTO `admin` VALUES (4, '1', '8cbddd7fe559890789706c524c4a451c', '1', '', 2, 1, 1, 1632809870541, 1632809870541, 0);
INSERT INTO `admin` VALUES (5, 'Jane2', '8cbddd7fe559890789706c524c4a451c', '1', '', 2, 1, 1, 1632809952498, 1632809952498, 0);

-- ----------------------------
-- Table structure for album
-- ----------------------------
DROP TABLE IF EXISTS `album`;
CREATE TABLE `album`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '专辑名称',
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `cover` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '封面',
  `sid` int(11) NOT NULL COMMENT '所属歌星',
  `isDel` int(11) NOT NULL COMMENT '是否删除 1是 0否',
  `status` int(11) NOT NULL COMMENT '状态 1显示 0隐藏',
  `createTime` bigint(13) NOT NULL COMMENT '创建时间',
  `updateTime` bigint(13) NOT NULL COMMENT '修改时间',
  `delTime` bigint(13) NOT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of album
-- ----------------------------
INSERT INTO `album` VALUES (1, '12123', '21321', '21341', 1, 0, 1, 1632985722165, 1632990036582, 1632990160288);

-- ----------------------------
-- Table structure for banner
-- ----------------------------
DROP TABLE IF EXISTS `banner`;
CREATE TABLE `banner`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `identity` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '唯一标识',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '广告名称',
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `banners` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '广告内容',
  `status` int(11) NOT NULL COMMENT '状态 1显示 0隐藏',
  `isDel` int(11) NOT NULL COMMENT '是否删除 1是 0否',
  `createTime` bigint(13) NOT NULL COMMENT '创建时间',
  `updateTime` bigint(13) NOT NULL COMMENT '修改时间',
  `delTime` bigint(13) NOT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of banner
-- ----------------------------
INSERT INTO `banner` VALUES (1, 'banner', '1112', '222', '[{\\\"title\\\":\\\"标题1\\\",\\\"desc\\\":\\\"描述1\\\",\\\"image\\\":\\\"图片1\\\",\\\"link\\\":\\\"跳转链接1\\\"},{\\\"title\\\":\\\"标题2\\\",\\\"desc\\\":\\\"描述2\\\",\\\"image\\\":\\\"图片2\\\",\\\"link\\\":\\\"跳转链接2\\\"}]', 1, 0, 1632900121366, 1632901192777, 0);
INSERT INTO `banner` VALUES (2, 'banner2', '1112', '222', '[{\\\"title\\\":\\\"标题1\\\",\\\"desc\\\":\\\"描述1\\\",\\\"image\\\":\\\"图片1\\\",\\\"link\\\":\\\"跳转链接1\\\"},{\\\"title\\\":\\\"标题2\\\",\\\"desc\\\":\\\"描述2\\\",\\\"image\\\":\\\"图片2\\\",\\\"link\\\":\\\"跳转链接2\\\"}]', 1, 0, 1632902130033, 1632902130033, 0);

-- ----------------------------
-- Table structure for classify
-- ----------------------------
DROP TABLE IF EXISTS `classify`;
CREATE TABLE `classify`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类名',
  `sort` int(11) NOT NULL COMMENT '排序',
  `status` int(11) NOT NULL COMMENT '状态 1显示 0隐藏',
  `isDel` int(11) NOT NULL COMMENT '是否删除 1是 0否',
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `createTime` bigint(13) NOT NULL COMMENT '创建时间',
  `updateTime` bigint(13) NOT NULL COMMENT '修改时间',
  `delTime` bigint(13) NOT NULL COMMENT '删除时间',
  `cover` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '封面',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of classify
-- ----------------------------
INSERT INTO `classify` VALUES (1, '1112', 2, 1, 0, '222', 1632903671819, 1632903731058, 0, '12121');
INSERT INTO `classify` VALUES (2, '333', 0, 1, 0, '333', 1642903671819, 16432903731058, 0, NULL);

-- ----------------------------
-- Table structure for collection
-- ----------------------------
DROP TABLE IF EXISTS `collection`;
CREATE TABLE `collection`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `cid` int(11) NOT NULL COMMENT '收藏对象id',
  `type` int(11) NOT NULL COMMENT '收藏类型 1歌曲 2歌星 3专辑 4歌单',
  `uid` int(11) NOT NULL COMMENT '收藏者id',
  `createTime` bigint(13) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of collection
-- ----------------------------
INSERT INTO `collection` VALUES (2, 1, 1, 1, 1633754392694);

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `cid` int(11) NOT NULL COMMENT '评论对象id',
  `pid` int(11) NOT NULL COMMENT '回复评论id',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '评论内容',
  `type` int(11) NOT NULL COMMENT '评论类型 1歌曲 2歌星 3专辑 4歌单',
  `uid` int(11) NOT NULL COMMENT '评论者id',
  `isDel` int(11) NOT NULL COMMENT '是否删除 1是 0否',
  `createTime` bigint(13) NOT NULL COMMENT '创建时间',
  `updateTime` bigint(13) NOT NULL COMMENT '修改时间',
  `delTime` bigint(13) NOT NULL COMMENT '删除时间',
  `status` int(13) NOT NULL COMMENT '状态 1显示 0隐藏',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES (1, 1, 0, 'type1', 3, 1, 0, 1, 1633141727154, 1633141736110, 1);
INSERT INTO `comment` VALUES (2, 1, 1, 'type1c', 3, 1, 0, 1, 1, 1633141736110, 1);
INSERT INTO `comment` VALUES (3, 1, 2, 'type2c', 3, 1, 0, 1, 1, 1633141736110, 1);
INSERT INTO `comment` VALUES (4, 1, 0, '评论', 3, 1, 0, 1633750190144, 1633750190144, 0, 1);
INSERT INTO `comment` VALUES (5, 1, 0, '评论', 2, 1, 0, 1633761017656, 1633761017656, 0, 1);
INSERT INTO `comment` VALUES (6, 1, 0, '评论2', 3, 1, 0, 1633766217010, 1633766217010, 0, 1);

-- ----------------------------
-- Table structure for emailcode
-- ----------------------------
DROP TABLE IF EXISTS `emailcode`;
CREATE TABLE `emailcode`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '验证邮箱',
  `code` int(255) NOT NULL COMMENT '验证码',
  `type` int(11) NOT NULL COMMENT '验证类型 1注册 2找回密码',
  `verifyTime` bigint(13) NOT NULL COMMENT '过期时间',
  `pushTime` bigint(13) NOT NULL COMMENT '发送时间',
  `isInvalid` int(11) NOT NULL COMMENT '是否作废 1是 0否',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of emailcode
-- ----------------------------
INSERT INTO `emailcode` VALUES (1, 'sn18253220107@126.com', 236602, 1, 1633681324569, 1633679524569, 1);
INSERT INTO `emailcode` VALUES (2, 'sn18253220107@126.com', 341373, 1, 1633681736921, 1633679936921, 1);
INSERT INTO `emailcode` VALUES (3, 'sn18253220107@126.com', 448447, 1, 1633681763514, 1633679963514, 1);
INSERT INTO `emailcode` VALUES (4, 'sn18253220107@126.com', 136112, 1, 1633681833681, 1633680033681, 1);
INSERT INTO `emailcode` VALUES (5, 'sn18253220107@126.com', 934382, 1, 1633681834331, 1633680034331, 1);
INSERT INTO `emailcode` VALUES (6, 'sn18253220107@126.com', 976255, 1, 1633682320205, 1633680520205, 1);
INSERT INTO `emailcode` VALUES (7, 'sn18253220107@126.com', 233332, 1, 1633682526950, 1633680726950, 1);
INSERT INTO `emailcode` VALUES (8, 'sn18253220107@126.com', 345233, 1, 1633683433260, 1633681633260, 0);
INSERT INTO `emailcode` VALUES (9, 'sn18253220107@126.com', 42251, 2, 1633701035289, 1633699235289, 1);
INSERT INTO `emailcode` VALUES (10, 'sn18253220107@126.com', 546089, 2, 1633701096215, 1633699296215, 1);
INSERT INTO `emailcode` VALUES (11, 'sn18253220107@126.com', 515778, 2, 1633701477389, 1633699677389, 1);
INSERT INTO `emailcode` VALUES (12, 'sn18253220107@126.com', 631381, 2, 1633702452092, 1633700652092, 1);
INSERT INTO `emailcode` VALUES (13, 'sn18253220107@126.com', 112371, 2, 1633702604087, 1633700804087, 1);
INSERT INTO `emailcode` VALUES (14, 'sn18253220107@126.com', 193153, 2, 1633702668860, 1633700868860, 1);
INSERT INTO `emailcode` VALUES (15, 'sn18253220107@126.com', 766800, 2, 1633702731019, 1633700931019, 1);
INSERT INTO `emailcode` VALUES (16, 'sn18253220107@126.com', 886864, 2, 1633702777588, 1633700977588, 1);
INSERT INTO `emailcode` VALUES (17, 'sn18253220107@126.com', 233884, 2, 1633703193203, 1633701393203, 1);
INSERT INTO `emailcode` VALUES (18, 'sn18253220107@126.com', 765483, 2, 1633703257524, 1633701457524, 1);
INSERT INTO `emailcode` VALUES (20, 'sn18253220107@126.com', 781004, 2, 1633703486831, 1633701686831, 1);
INSERT INTO `emailcode` VALUES (21, 'sn18253220107@126.com', 189438, 2, 1633703530805, 1633701730805, 1);
INSERT INTO `emailcode` VALUES (22, 'sn18253220107@126.com', 769937, 2, 1633703578551, 1633701778551, 1);
INSERT INTO `emailcode` VALUES (23, 'sn18253220107@126.com', 779586, 2, 1633703586888, 1633701786888, 1);
INSERT INTO `emailcode` VALUES (24, 'sn18253220107@126.com', 814626, 2, 1633703589828, 1633701789828, 1);
INSERT INTO `emailcode` VALUES (25, 'sn18253220107@126.com', 375783, 2, 1633703894065, 1633702094065, 0);

-- ----------------------------
-- Table structure for likes
-- ----------------------------
DROP TABLE IF EXISTS `likes`;
CREATE TABLE `likes`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `lid` int(11) NOT NULL COMMENT '点赞对象id',
  `uid` int(11) NOT NULL COMMENT '点赞者id',
  `type` int(11) NOT NULL COMMENT '点赞类型 1歌曲 2歌星 3专辑 4歌单 5评论',
  `createTime` bigint(13) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of likes
-- ----------------------------
INSERT INTO `likes` VALUES (10, 1, 1, 5, 1633761220491);
INSERT INTO `likes` VALUES (11, 1, 1, 3, 1);

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `api` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '后端路由',
  `identity` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '唯一标识',
  `isDel` int(11) NOT NULL COMMENT '是否删除 1是 0否',
  `createTime` bigint(13) NOT NULL COMMENT '创建时间',
  `updateTime` bigint(13) NOT NULL COMMENT '修改时间',
  `delTime` bigint(13) NOT NULL COMMENT '删除时间',
  `pid` int(11) NOT NULL COMMENT '父级id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, '测试模块3', '测试模块', '/admin/menu/list', 'aa', 0, 1632753581103, 1632754306564, 0, 0);
INSERT INTO `menu` VALUES (2, '测试模块2', '测试模块', '111', 'bb', 0, 1632754212920, 1632754212920, 1633136421958, 1);
INSERT INTO `menu` VALUES (3, '测试模块1', '测试模块', '111', 'cc', 0, 1632754297692, 1632754297692, 1633136421958, 2);
INSERT INTO `menu` VALUES (4, 'dsafas', 'sdafdsf', '111', 'dd', 0, 1, 1, 0, 0);

-- ----------------------------
-- Table structure for music
-- ----------------------------
DROP TABLE IF EXISTS `music`;
CREATE TABLE `music`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '歌曲名',
  `sid` int(11) NULL DEFAULT NULL COMMENT '所属歌星',
  `aid` int(11) NULL DEFAULT NULL COMMENT '所属专辑',
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '简介',
  `cover` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '封面',
  `music` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '歌曲链接',
  `cid` int(11) NOT NULL COMMENT '所属分类',
  `lyrics` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '歌词',
  `status` int(11) NOT NULL COMMENT '显示隐藏 1显示 0隐藏',
  `isDel` int(11) NOT NULL COMMENT '是否删除 1是 0否',
  `createTime` bigint(13) NOT NULL COMMENT '创建时间',
  `updateTime` bigint(13) NOT NULL COMMENT '修改时间',
  `delTime` bigint(13) NOT NULL COMMENT '删除时间',
  `playNum` bigint(13) NOT NULL COMMENT '播放数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of music
-- ----------------------------
INSERT INTO `music` VALUES (1, 'sdafasd2', 1, 1, '', '', 'dsafsadfsad', 1, '[{\\\"start\\\":\\\"00:00\\\",\\\"end\\\":\\\"00:10\\\",\\\"text\\\":\\\"歌词段落1\\\"},{\\\"start\\\":\\\"00:11\\\",\\\"end\\\":\\\"00:31\\\",\\\"text\\\":\\\"歌词段落2\\\"}]', 1, 0, 1633013692173, 1633013762987, 1633013789806, 18);
INSERT INTO `music` VALUES (2, 'dsaf', 2, 2, NULL, NULL, 'asdf', 1, NULL, 1, 0, 1, 1, 0, 8);

-- ----------------------------
-- Table structure for playlist
-- ----------------------------
DROP TABLE IF EXISTS `playlist`;
CREATE TABLE `playlist`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '歌单名称',
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `cover` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '封面',
  `status` int(11) NOT NULL COMMENT '是否显示 1是 0否',
  `private` int(11) NOT NULL COMMENT '是否私有 1私有 0公开',
  `musics` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '歌曲集',
  `isDel` int(11) NOT NULL COMMENT '是否删除 1是 0否',
  `uid` int(11) NOT NULL COMMENT '所属用户id',
  `playNum` bigint(13) NOT NULL COMMENT '播放量',
  `createTime` bigint(13) NOT NULL COMMENT '创建时间',
  `updateTime` bigint(13) NOT NULL COMMENT '修改时间',
  `delTime` bigint(13) NOT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of playlist
-- ----------------------------
INSERT INTO `playlist` VALUES (1, '121', '123', '123', 1, 0, '[]', 0, 1, 0, 1, 1634008247955, 1633094423590);
INSERT INTO `playlist` VALUES (2, '新歌单', 'asdfasd', '', 1, 1, '[]', 1, 1, 0, 1634006079000, 1634006211125, 1634006591520);

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名',
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色简介',
  `power` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色权限',
  `createTime` bigint(13) NOT NULL COMMENT '创建时间',
  `updateTime` bigint(13) NOT NULL COMMENT '修改时间',
  `delTime` bigint(13) NOT NULL COMMENT '删除时间',
  `isDel` int(11) NOT NULL COMMENT '是否删除 1是 0否',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (1, '超级管理员', '超级管理员', '', 1632811865461, 1632811905860, 0, 0);
INSERT INTO `role` VALUES (2, '测试角色1', '测试角色', '1,2,3,4', 1632811915383, 1632811915383, 0, 0);
INSERT INTO `role` VALUES (3, '测试角色1', '测试角色', '', 1632811987082, 1632811987082, 0, 1);

-- ----------------------------
-- Table structure for star
-- ----------------------------
DROP TABLE IF EXISTS `star`;
CREATE TABLE `star`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '姓名',
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '简介',
  `headimg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `status` int(11) NOT NULL COMMENT '状态 1显示 0隐藏',
  `isDel` int(11) NOT NULL COMMENT '是否删除 1是 0否',
  `createTime` bigint(13) NOT NULL COMMENT '创建时间',
  `updateTime` bigint(13) NOT NULL COMMENT '修改时间',
  `delTime` bigint(13) NOT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of star
-- ----------------------------
INSERT INTO `star` VALUES (1, '11123', '222', '', 1, 0, 1632904773053, 1632904819232, 1632983299235);
INSERT INTO `star` VALUES (2, '21321', '1', NULL, 1, 0, 1, 1, 0);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '账号',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `nickname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '昵称',
  `headimg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '邮箱',
  `isDel` int(11) NOT NULL COMMENT '是否已删除 1是 0否',
  `status` int(11) NOT NULL COMMENT '状态 1正常 0封号',
  `createTime` bigint(13) NOT NULL COMMENT '创建时间',
  `updateTime` bigint(13) NOT NULL COMMENT '修改时间',
  `delTime` bigint(13) NOT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'sn18253220107@126.com', '7a184fc991737e2b4bf783526c35a587', '达文西', '', 'sn18253220107@126.com', 0, 1, 1633681649959, 1633699687472, 0);

SET FOREIGN_KEY_CHECKS = 1;
