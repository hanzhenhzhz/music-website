import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/home/Home'
import Star from '../pages/star/Star'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/Star',
      name: 'Star',
      component: Star
    }
  ]
})
